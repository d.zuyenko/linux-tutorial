**CONTENTS**

[[_TOC_]]


# **<div align="center">AWS (Amazon Web Services):</div>**
## **<div align="center">AWS Cloud Computing:</div>**
**Six advantages of cloud computing:</div>**
- **Trade capital expense (CAPEX) for operational expense (OPEX)** *(Trade fixed expense for variable expence)*:
    - Pay On-Demand: don't own hardware;
    - Reduced Total Cost of Ownership (TCO) & Operational Expense (OPEX);
- **Benefit from massive economies of scale**:
    - Prices are reduced as AWS is more efficient due to large scale;
- **Stop guessing capacity**:
    - Scale based of actual measured usage;
- **Increase speed and agility**;
- **Stop spending money running and maintaining data centers**;
- **Go global in minutes**: leverage the AWS global infrastructure.


**Problems solved by the cloud:**
- Flexiblity: change resource types when needed;
- Cost-Effectiveness: pay as you go, for what you use;
- Scalability: accommodate latger loads loads by making hardware stronger or adding additional nodes;
- Elasticity: ability to scale out and scale-in when needed;
- High-availability and fault-tolerance: build across data centers;
- Agility: rapidlt develop, test and launch software applications.


**<div align="center">Well-Architected Framework:</div>**
**General Guiding Principles**:
- Stop guessing your capacity needs;
- Test systems at production scale;
- Automate to make architectural experimentation easier;
- Allow for evolutionary architechtures;
    - Design based on changing requirements;
- Drive architectires using data;
- Improve through game days;
    - Simulate applications for flash sales days.


**AWS Cloud Best Practices - Design Principles**:
- Scalability: vertical & horizontal;
- Disposable Resourcs: servers should be disposable & easily configured;
- Automation: Serverless, Infrastructure as a Service, Auto Scaling..;
- Loose Coupling: change or failure in one component should not cascade to other;
- Services, not Servers: use managed services, databases and serverless, instead of just EC2.


**Well-Architected Framework (6 Pillars):**
- **Operational Excelence**: *Prepare, Operate, Evolve.* Includes the ability to run and monitor systems to deliver business value and to continually improve upporting processes and procedures:
    - Perform operations as code (IaaS);
    - Make frequent, small, recersible changes: So in a case of failure, it can be reversed;
    - Refine operations procedures frequently: Ensure that team members are familiar with it;
    - Anticipate failure;
    - Learn from all operational failures;
    - Use managed services: to reduce operational burden;
    - Implement observability for actionable insights: performance, reliability, cost, etc;
- **Security**: *Identity and Access Management, Detective Controls, Infrastructure Protection, Data Protection, Incident Response.* Includes the ability to protect information, systems and assets while delivering business value through risk assessments and mitigation strategies:
    - Implement a strong identity foundation: Centralize privilege management and reduce reliance on long-term credentials. Principle of least privilege;
    - Enable traceability: Integrate logs and metrics with systems to automatically respond and take action;
    - Apply security at all layers: Like edge network, VPC, subnet, load balancer, every instance, operating system and application;
    - Automate security best practices;
    - Protect data in transit and at rest: Encryption, tokenization and access control;
    - Keep people away from data: Reduce incident response simulations and use tools with automation to increase your speed for detectionm inverstigation and recovery;
    - Shared Responsibility Model;
- **Reliability**: *Foundations, Change Management, Failure Management.* Ability of a system to recover from infrastructure or service disruptions, dynamically acquire computing resources to meet demand and mitigate disruptions such as misconfigurations or transient network issues:
    - Test recovery procedures: Use automation to simulate different failures or to recreate scenarios that led to failures before;
    - Automatically recover from failure: Anticipate and remediate failures before they occur;
    - Scale hoizontally to increase aggregate system availability: Distribute requests across multiple, smaller resources to ensure that they don't share a common point of failure;
    - Stop guessing capacity: Maintain the optimal level to stisfy demand without over or under provisioning: use auto scaling;
    - Manage change in automation: Use automation to make changes to infrstructure;
- **Performance Efficiency**: *Selection, Review, Monitoring, Tradeoffs*. Includes the ability to use computing resources efficiently to meet system requirements, and to maintain that efficiency as demand changes and technologies evolve:
    - Democratize advanced technologies: Advance technologies become services and hence you can focus more on product development;
    - Go global in minutes: Easy deployment in multiple regions;
    - Use serverless architectures: Avoid burden of managing servers;
    - Experiment more often: Easy to carry our comparative testing;
    - Mechanical sympathy: Be aware of all AWS services;
- **Cost Optimization**: *Expenditure Awareness, Cost Efective Resources, Matching supply and demand, Optimizing Over Time*. Includes the ability to run systems to deliver business value at the lowest price point:
    - Adopt a onsumption mode: Pay only for what you use;
    - Measure overall efficiency: use CloudWatch;
    - Stop spending money on data center operations: AWS does the infrastructure art and enables customer to focus on organization projects;
    - Analyze and attribute expenditure: Accurate identification of system usage and costs, helps measure return on inverstment (ROI): Make sure to use tags;
    - Use managed and application level services to reduce cost of ownership: As managed services operate at cloud scale, they an offer a lower cost per transaction or service;
- **Sustainability**: *EC2 Auto Scaling, Lambda and Fargate, Cost Explorer, EC2 Spot Instances, EFS-IA, Amazon S3 Glacier, Amazon Data Lifecycle Manager, Read Local, Write Global (RDS Replical, DynamoDB Global table, CloudFront).* The sustainability pillar focuses on minimizing the environmental impact of running cloud workloads:
    - Understand your impact: establish performance indicators, evaluate improvemtns;
    - Establish sustainability goals: Set long-term goals for each workloadm model return in investments (ROI);
    - Maximize utilization: Right size each workload to maximize the energy efficiency of the underlying hardware and minimize idle resources;
    - Anticipate and adopt new, more efficient hardware and software offering: and design for flexibility to adopt new technologies over time;
    - Use managed services: Shared services reduce the amount of infrastructure; Managed services help automate sustainability best practices as moving infrequent accessed data to cold storage and adjusting compute capacity;
    - Reduce the downstream impact of your cloud workloadds: Reduce the amount of energy or resources required to use your services and reduce the neeed for your customers to upgrade their devices.
*They are not something to balance or trade-offs, they are a synergy.*


**AWS Customer Carbon Footprint Tool**: track, measure, review and forecast the Carbon emmisions generated from your AWS usage. Helps you meet your own sustainability goals. 


**AWS Cloud Adoption Framework (AWS CAF)** helps you build and then execute a comprehensive plan for your digital transformation through innovating use of AWS.


**<div align="center">AWS Cloud Adoption Framework (AWS CAF)</div>**
**The AWS Cloud Adoption Framework (AWS CAF)** leverages AWS experience and best practices to help you digitally transform and accelerate your business outcomes through innovative use of AWS. AWS CAF identifies specific organizational capabilities that underpin successful cloud transformations. These capabilities provide best practice guidance that helps you improve your cloud readiness.
**Six perspectives:**
- **Business Perspective**: helps ensure that your cloud investments accelerate your digital transformation abitions and business outcomes;
- **People Perspective**: serve as a bridge between technology and business, accelerating the cloud journey to help organizations more rapidly evolve to a culture of continuous growth, learning and where change becomes business-as-normal, with focus on culture, organizational structure, leadership and workforce;
- **Governance Perspective**: helps you orchestrate your cloud initiatives while maximizing organizational benefits and minimizing transformation-related risks;
- **Platform Perspective**: helps you build an enterprise-grade, sclable, hybrid cloud platform; modernize existing workloads; and implement new cloud-native solutions;
- **Security Perspective**: helps you achive the confidentiality, integrity and availability of your data and cloud workloads;
- **Operations Perspective**: helps ensure that your cloud services are delivered at a level that meets the needs of your business.


**AWS CAF - Transformation Domains**:
- Technology: using the cloud to migrate and modernize legacy infrastructure, aplications, data and analyticas platforms;
- Process: digitizing, automating and optimizing your business operations:
    - Leveraging new data ad analytics platforms to create actionable insights;
    - Using machine learning (ML) to improve your customer service experience;
- Organization: Reimagining your operating model:
    - Organizing your teams around products and value streams;
    - leveraging agile methods to rapidly iterate and evolve;
- Product: reimagining your business model by creating new value propositions (products & services) and revenue models.


**AWS Right sizing**: is the process of matching instance types and sizes to your workload performance and capacity requirements at lowest possible cost.


**<div align="center">AWS Professional Services & Partner Network</div>**:
- **APN Technology Partners**: Independent Software Vendors (ISVs), tools providers, platform providers, and others;
- **APN Consulting Partners**: System Integrators (SIs), agencies, consultancies, Managed Service Providers (MSPs), and others;
- **APN Training Partners**: a breadth of AWS Training options for learners of all levels, also provides classroom and digital offerings, and live instructors, on-demand courses. Finds who can help you learn AWS.


**AWS IQ**: quickly find professioal help for your AWS projects. Engage and pay AWS Certified third-party experts for on-demand project work. Video-conferencing, contract management, secure collaboration, integrated billing.


**AWS re:Post**: AWS-managed Q&A service.


**AWS Managed Services (AMS)** provides infrastructure and application support on AWS. Offers a team of AWS experts who manage and operate your infrastructure for security, reliability and abailability.


## **<div align="center">AWS Global Infrastructure:</div>**
**AWS Regions**: is a physical location around the world where we cluster data centers. We call each group of logical data centers an Availability Zone. Each AWS Region consists of a minimum of three, isolated, and physically separate AZs within a geographic area. 
- AWS has Regions all around the world;
- Names can be us-east-1, eu-west-2..;
- A region is a cluster of data centers;
- Most AWS services are region-scoped.


**How to choose an AWS Region**:
- **Compliance** with data governnance and legal requirements: data never leaves a region without your explicit permission;
- **Proximity** to customers: reduced latency;
- **Available services** with a Region: new services and new features aren't available in every Region;
- **Pricing**: pricing varies region to region and is transparent in the service pricing page.


**AWS Availability Zones (AZ)**: is one or more discrete data centers with redundant power, networking, and connectivity in an AWS Region. AZs give customers the ability to operate production applications and databases that are more highly available, fault tolerant, and scalable than would be possible from a single data center. 
- Each region has many availability zones (min 3, max 6): ap-southeast-2a, ap-southeast-2b, ap-southeast-2c;
- Each availability zone (AZ) is one or more discrete data centers with redundant power, networking and connectivity;
- They're separate from each other, so that they're isolated from disasters;
- They're connected with high bandwidth, ultra-low latency networking.


**AWS Local Zone** location is an extension of an AWS Region where you can run your latency sensitive applications using AWS services such as Amazon Elastic Compute Cloud, Amazon Virtual Private Cloud, Amazon Elastic Block Store, Amazon File Storage, and Amazon Elastic Load Balancing in geographic proximity to end-users.


**AWS Edge Locations** *(Point of Presence)*: is a site that Amazon CloudFront uses to store cached copies of your content closer to your customers for faster delivery.
- Amazon has 400+ Points of Presence (400+ Edge Locations & 10+ Regional Caches) in 90+ cities across 40+ countries;
- Content is delivered to end users with lower latency.


**AWS WaveLenght** are infrastructure deplaoyments, embedded within the telecommunications providers' datacenters at the edge of the 5G networks.


**AWS Outposts** are "server racks" that offers the same AWS infrastructure, services, APIs & tools to build your own applications on-premises just as in the cloud. AWS will setup and namage "Outpost racks" within your on-premises infrastructure. Customer is responsible of the Outposts Rack physical security.
- Low-latency access to on-premises systems;
- Local data processing;
- Data residency;
- Easier migration from on-premises to the cloud;
- Fully managed service.


**Service**: AWS offers a broad set of global cloud-based products including compute, storage, database, analytics, networking, machine learning and AI, mobile, developer tools, IoT, security, enterprise applications, and much more.


**Tools to access AWS Services:**
- **AWS Management Console**: WEB-UI management tool (protected by password + MFA);
- **AWS CLI**: direct access to the public APIs of AWS services via command-line shell;
- **AWS SDK**: language specific APIs (set of libraries), enables access and manage AWS services programmatically (embedded in application).


## **<div align="center">AWS Shared Responsibility Model:</div>**
- **Customer responsibility** for the security ***IN*** the cloud: Customers are responsible for the security of everything that they create and put in the AWS Cloud;
    - EC2 instances:
        - Customer is responsible for management of the guest OS (including security patches and updates), firewall & network configuration, IAM;
        - Encrypting application data.
    - S3:
        - Bucket configuration;
        - Bucket policy / public setting;
        - IAM user and roles;
        - Enabling encryption.
- **AWS responsibility** for the security ***OF*** the cloud: AWS operates, manages, and controls the components at all layers of infrastructure;
    - Protecting infrastructure (hardware, software, facilities and networking):
    - Managed services (like S3, DynamoDB, RDS, etc);
    - S3:
        - Guarantee unlimited storage;
        - Guarantee encryption;
        - Ensure separation of the data between different cutomers;
        - Ensure AWS employees can't access customer's data.
- **Shared controls**:
    - Patch management, Configuration Management, Awareness & Training.


## **<div align="center">AWS Identity and Access Manageent:</div>**
**IAM (Identity and Access Management)** enables you to securely control access to Amazon Web Services services and resources for your users.<br>
Users are people within your organization, and can be grouped. Users don't have to belong to a group, and user can belong to multiple groups. Groups only contain users, not other groups.
*Root privileges has complete access to all AWS services and resources. Root account created by default.*


**Actions that can be performed only by the root user**:
- Change account settings (account name, email address, root user password, root user access keys);
- View certain tax invoices;
- Close your AWS account;
- Restore IAM user permissions;
- Change or cancel AWS Support plan;
- Register as a seller in the Reserved Instance Marketplace;
- Configure an Amazon S3 bucket to enable MFA;
- Edit or delete an Amazon S3 bucket policy that includes an invalid VPC ID or VPC endpoint ID;
- Sign up for GovCloud.


**Policies** define the permissions of the users and grouops described in JSON documents.


**IAM Roles for services** set of permission attached to some AWS services to perform actions on your behalf (EC2, Lambda, CloudFormation).


**Best IAM practices**:
- **Root** account shouldn't be used or shared;
- In **IAM Policies** apply the least priviledge principle: don't give more permissions than a user needs;
- Create individual **IAM users** for each person who needs to access AWS. One user - one physical user;
- Assign users to groups and assign permissions to **Groups**;
- **IAM roles** are ideal for situations in which access to services or resources needs to be granted temporarily, instead of long-term.


**IAM Credentials Report (account-level)** a report that lists all your account's users and the status of their various;
IAM Access Advisor (user-level) - access advisor shows the service permissions granted to a user and when those services were last accessed. Can be used to revise policies.


**Service Control Policies (SCP)** service to manage the security and permissions that are available to your organization's AWS accounts. To restrict to access to certain services.
- Whitelist or blacklist IAM actions;
- Applied at the OU or Account level;
- Does not apply to the Master Account;
- SCP is applied to all the Users and Roles of the Account, including Root user;
- The SCP doesn't affect service-linked roles;
- SCP must have explicit Allow (does not allow anything by default).


**AWS Organization** is a global service that alows to manage multiple AWS accounts. Automate AWS account creation by available API.
*Restrict account privileges using **Service Control Policies (SCP)***.
- Consolidated Billing across all accounts;
- Pricing benefits from aggregated usage (volume discount for EC2, S3, etc);
- Pooling of Reserved EC2 instances for optimal savings.


**Multi Account Strategies**:
- Create account per departament, per cost center or per dev/test/prod, based on regulatory  restrictions (using SCP), for better resource isolation (VPC), to have separate per-account service limits, isolated account for logging.
- Multi Account vs One Account Multi VPC;
- Use tagging standards for billing puposes;
- Enables CloudTrail on all accounts, send logs to central S3 account;
- Send CloudWatch Logs to central logging account.


**AWS Organization - Consolidated Billing** provides:
- Combined Usage: combine the usage across all AWS accounts in the AWS Organization to share the volume pricing, Reserved Instances and Saving Plans discounts.
- One Bill: get one bill for all AWS Accounts in the AWS Organization.


**AWS Control Tower** service that offers the easiest way to set up and govern a secure, multi-account AWS environment. It establishes a landing zone that is based on best-practices blueprints, and it enables governance using controls you can choose from a pre-packaged list.
*AWS Control Tower runs on top of AWS Organization* <br>
- Automate the set up of your environment in a few clicks;
- Automate ongoing policy management using guardrails;
- Detect policy violations and remediate them;
- Monitor compliance through an interactive dashboard.


**AWS IAM Identity Center**: one login (single sign-on) for all your: AWS accounts in AWS Organizations, Business cloud applications (Salesforce, Box, Microsoft 365, etc), SAML2.0-enabled applications, EC2 Windows Instances.


**AWS Resource Access Manager (AWS RAM)** helps you securely share your resources across AWS accounts, within your organization or organizational units (OUs) and with IAM roles and users for supported resource types (Aurora, VPC Subnets, Transit Gateway, Route53, EC2 Dedicated Hosts, License Manager Configurations, etc). Avoid resource duplication. 


**AWS Service Catalog** self-portal to launch a set of authorized products pre-defined by admins.


**AWS STS (Security Token Service)**: enables you to create temporary, limited-privileges credentials to access your AWS resources.


**Amazon Cognito**: enables you to create identities for your Web and Mobile applications users (potentially millions). Instead of creating them an IAM user, you create a user in Cognito.


**AWS Directory Services:**
- **AWS Managed Microsoft AD**: create your own AD in AWS, manage users locally, supports MFA. Establish "trust" connections with your on-premise AD.
- **AD Connector**: Directory Gateway (proxy) to redirect to on-premoses AD, supports MFA. Users are managed on the on-premise AD.
- **Simple AD**: AD-compatible managed directory on AWS. Cannot be joined with on-premise AD.


## **<div align="left">Amazon VPC:</div>**
**VPC (Virtual Private Cloud)** is a service that lets you launch AWS resources in a logically isolated virtual network that you define.<br>


**IP Addresses**:
- IPv4 (4.3 billion addresses);
- IPv6 (3.4 x 10 in 38 addresses);
- Elastic IP: allows you to attach a fixed public IPv4 address to EC2 instance.


**Subnets** allows you to partition your network inside VPC. <br>
**Public subnet** is a subnet that is accessible from the internet. <br>
**Private subnet** is a subnet that is not accessible from the internet. <br>
**Route Tables** defines access to the internet and between subnets.


**Internet gateway** helps our VPC instances connect with the internet (public subnets have a route to the internet gateway). <br>
**NAT Gateways (AWS-managed)** & **NAT Instances (self-managed)** allow your instances in your private subnets to access the internet while remaining private.


**Network ACL (NACL)** a firewall which controls traffic from and to subnet. Can have ALLOW and DENY rules. Are attched to the Subnet level. Rules only include IP addresses. Automatically applies to all instances in the subnets it's associated with (you don't have to rely on users to specify the security group). <br>
*It's STATELESS: Return traffic must be explicitly allowed by rules. Remember nothing and check packets that cross the subnet border each way: inbound and outbound.*
**Security Groups** a firewall that controls traffic to and from an EC2 Instance. Can have only ALLOW rules. Rules include IP addresses and other security groups. Applies to an instance only if someone specifies the security group when launching the instance or associates the security group with the instance later on. <br>
*It's STATEFUL: Return traffic is automatically allowed, regardless of any rules. They remember previous decisions made for incoming packets.*


**VPC Flow Logs** capture information about IP traffic going into your interfaces:
- VPC Flow Logs;
- Subnet Flow Logs;
- Elastic Network Interface Flow Logs;
- Network information from AWS managed interfaces: Elastic Load Balancers, ElastiCache, RDS, Aurora, etc.
*VPC Flow Logs data can go to S3, CloudWatch Logs and Kinesis Data Firehose.*


**VPC Peering** connect two VPC, privately using AWS' network, make them behave as if they were in the same network. Must not have. overlapping CIDR (IP address range). VPC Peering connection is not transitive (each VPC should be connected to another).


**VPC Endpoints** allow to connect to AWS Services using a private network instead of the public network. This gives enchances security and lower latency to access AWS services (AWS S3, AWS DynamoDB, etc).


**AWS PrivateLink (VPC Endpoint Services)** connects 3rd party VPC (via it's Network Load Balancer) and customer VPC (via it's Elastic Network Interface).


**Site to Site VPN** connect an on-premises VPN using **Customer Gateway (CGW)** to AWS using **Virtual Private Gateway (VPG)** over the Internet (encrypted).
- **Customer gateway device** is a physical or software appliance that you own or manage in your on-premises network (on your side of a Site-to-Site VPN connection);
- **Virtual private** gateway enables you to establish a virtual private network (VPN) connection between your VPC and a private network, such as an on-premises data center or internal corporate network. A virtual private gateway allows traffic into the VPC only if it is coming from an approved network.


**Direct Connect (DX)** is a service that lets you to establish a dedicated private connection (physical onnection) between on-premises and AWS over private line (private, secure and fast). <br>
*Takes a month to establish.*


**AWS Client VPN** connects over the Internet customer's end-device using OpenVPN and private network in AWS or on-premises. Allows to connect to EC2 using private IP.


**Transit Gateway** allows having transitive peering between numerous VPCs and on-premises (in a star like hub). Supports Direct Connect Gateway, VPN Connection.


## **<div align="left">Amazon Route53:</div>**
**AWS Route53** is a managed DNS (Domain Name System), collection of rules and records which helps clients understand how to reach a server through URLs.

**Route53 policies**:
- **Simple routing policy**: Use for a single resource that performs a given function for your domain, for example, a web server that serves content for the example.com website. You can use simple routing to create records in a private hosted zone;
- **Failover routing policy**: Use when you want to configure active-passive failover. You can use failover routing to create records in a private hosted zone.
- **Geolocation routing policy**: Use when you want to route traffic based on the location of your users. You can use geolocation routing to create records in a private hosted zone;
- **Geoproximity routing policy**: Use when you want to route traffic based on the location of your resources and, optionally, shift traffic from resources in one location to resources in another location. You can use geoproximity routing to create records in a private hosted zone;
- **Latency routing policy**: Use when you have resources in multiple AWS Regions and you want to route traffic to the Region that provides the best latency. You can use latency routing to create records in a private hosted zone;
- **IP-based routing policy**: Use when you want to route traffic based on the location of your users, and have the IP addresses that the traffic originates from;
- **Multivalue answer routing policy**: Use when you want Route 53 to respond to DNS queries with up to eight healthy records selected at random. You can use multivalue answer routing to create records in a private hosted zone;
- **Weighted routing policy**: Use to route traffic to multiple resources in proportions that you specify. You can use weighted routing to create records in a private hosted zone.


**AWS CloudFront** is a Content Delivery Network (CDN), improves read performance, content is cached at the edge. 


**AWS Global Accelerator** service that improves global applicaton availability and performance using the AWS global network. By leverage the AWS internal network to optimize the route to your application (up to 60% improvement). 


## **<div align="center">AWS EC2 (Elastic Compute Cloud)</div>**
**EC2 (Elastic Compute Cloud)** is virtual computer (instance) in the cloud.<br>
**EC2 consists**:
- Renting virtual machine (EC2);
- Storing data on virtual machine (EBS) and ;
- Distributing load across machines (ELB);
- Scaling the services using an auto-scaling group (ASG).


**EC2 configuration options**:
- Operating System (OS): Linux, Windows or Mac OS;
- How much compute power & cores (CPU);
- How much random-access memory (RAM);
- How much storage space:
    - Network-attached (EBS & EFS);
    - Hardware (EC2 Instance Store).
- Network card: speed of the card, Public IP address;
- Bootstrap script (launchinh commands when a machine starts for the first time): EC2 User Data - Installing updates, Downloading files and etc.


**EC2 Instance Types**:
- **General Purpose**: provides a balance of compute, memory, and networking resources: 
    - application servers;
    - gaming servers;
    - backend servers for enterprise applications;
    - small and medium databases;
- Compute Optimized: processing workloads, media transcoding, high-perforance web servers, machine learning and etc;
    - high-performance web servers;
    - compute-intensive applications servers;
    - dedicated gaming servers
    - batch processing workloads (that require processing many transactions in a single group);
- Memory Optimized: real-time processing of large unstructured in-memory data sets (preloaded in memory before running an application);
    - high-performance database;
    - real-time processing of a large amount of unstructured data;
- Accelerated Computing: use directly on hardware accelerators, or coprocessors, which is more efficiently than is possible in software running on CPUs:
    - functions with floating-point number calculations;
    - graphics processing;
    - data pattern matching;
- Storage Optimized: designed for workloads that require high, sequential read and write access to large datasets on local storage;
    - SQl and NoSQL databases;
    - data warehousing applications;
    - distributied file systems;
    - high-frequency online transaction processing (OLTP) systems;
- HPC Optimized: built to offer the best price performance for running HPC workloads at scale on AWS, for applications that benefit from high-performance processors;
    - large, complex simulations;
    - deep learning workloads.


**m5.2xlarge:**
- **m**: instance class;
- **5**: generation of hardware;
- **2xlarge**: size within the instance class.


**AMI (Aamazon Machine Image)** - customization of an EC2 instance, added ext. software, 
- A Public AMI: Amazon provided;
- Your own AMI: you make and maintain them yourself;
- An AWS Marketplace AMI: an AMI someone else made (and potentially sells).


**EC2 Image Builder** service to automate the creation, maintain, validate and test of Virtual Machine or container images. Can be run on schedule.


**Connect to EC2:**<br>
`ssh -i /<path>/<key_pair_name>.pem <instance_user_name>@<instance_public_dns_name/IP>`<br>
**_Example:_**<br>
`ssh -i /home/kali/Downloads/aws.pem ubuntu@51.20.123.211`<br>


## **<div align="center">Storage:</div>**
**Amazon EC2 instance store** provides temporary *block-level storage* for your instance. This storage is located on disks that are physically attached to the host computer. Instance store is ideal for temporary storage of information that changes frequently, such as buffers, caches, scratch data, and other temporary content. It can also be used to store temporary data that you replicate across a fleet of instances, such as a load-balanced pool of web servers.


**Amazon EBS (Elastic Block Store)** volume is a *block-level storage* (network drive) you can attach to your instances while they run. It allows instances to persist data, even after termination. <br>
*EBS can be mounted to one instance at a time. And bound to a specific avialability zone. (To move to another AZ it's possible to use EBS Snapshot).* <br>
*EBS Snapshot could be moved to Archive (that is 75% cheaper, but it takes within 24 to 72 hours for restoring the archive).*


**Amazon EFS (Elastic File System)** is a scalable file system used with AWS Cloud services and on-premises resources. As you add and remove files, Amazon EFS grows and shrinks automatically. It can scale on demand to petabytes without disrupting applications. In file storage, multiple clients (such as users, applications, servers, and so on) can access data that is stored in shared file folders. In this approach, a storage server uses block storage with a local file system to organize files. Clients access data through file paths. <br>
**Amazon EFS-IA (Elastic File System Infrequent Access)** cost-optimized storage class for files not accessed every day *(Up to 92% lower cost)*. Lifecycle Policy allow automatically move files from EFS to EFS-IA based on the last time they were accessed (no access for 60 days, for example).
*Unlike Amazon EBS, which stores data only in one AZ, Amazon EFS is a regional service. It stores data in and across multiple Availability Zones.*
*Compared to block storage and object storage, file storage is ideal for use cases in which a large number of services and resources need to access the same data at the same time.*

**Amazon FSx for Windows File Server** fully managed, highly reliable and scalable Windows native shared file system based on SMB protocol and Windows NTFS (Integrated with Microsoft Active Directory).


**Amazon FSx for Lustre (Linux cluster)** a fully managed high-performance, scalable file system for High Performance Computing (HPC): machine learning, analytics, video processing and financial modeling.


**AWS Storage Gateway:** bridge between on-premise data and cloud data in S3, hybrid storage service to allow on-premise to seamlessly use the AWS Cloud. Use cases: disaster recovery, backup & restore, tiered storage.


**Types of Storage Gateway**:
- **File Gateway**:
    - Amazon S3: a file interface that enables you to store files as objects in Amazon S3 using the industry-standard NFS and SMB file protocols;
    - Amazon FSx fully managed, highly reliable, and scalable file shares in the cloud using the industry-standard NFS and SMB protocols;
- **Volume Gateway**: applications' block storage volumes using the iSCSI protocol. Data written to these volumes can be asynchronously backed up as point-in-time snapshots of your volumes, and stored in the cloud as Amazon EBS snapshots. It's possible to back up on-premises Volume Gateway volumes using the service’s native snapshot scheduler or by using the AWS Backup service;
- **Tape Gateway**: an iSCSI-based virtual tape library (VTL) of virtual tape drives.


### **<div align="center">AWS S3:</div>**
**S3 (Simple Storage Service)** provides object storage through a web service interface.<br>
**Amazon S3:** allows to store objects (files) in 'buckets' (directories). <br>
*Amazon S3 offers unlimited storage space. The maximum file size for an object in Amazon S3 is 5 TB.*


**Naming convention**:
- No uppercase, No underscore;
- 3-63 characters long;
- Not an IP;
- Must start with lowercase letter or number;
- Must NOT start with the prefix **xn--**;
- Must NOT end with the suffix **-s3alias**.


**Objects** have a Key, wich is a full path to them (`s3://<bucket_name>/<folder_name>/<file-name>`). Max size of an Object is 5TB (5000GB), if uploading more than 5GB, should be used "multi-part upload".


**Amazon S3 Versioning** protects against unintended deletes. It is enabled at the bucket level.
Suspending versioning does not delete the previous versions.


**Amazon S3 Replication:**
- Cross-Region Replication (CRR);
- Same-Region Replication (SRR).
*Must enable Versioning.*


**S3 Storage Classes**:
- **Amazon S3 Standard - General Purpose**: used for frequently access data, low latency and high throughput, sustain 2 concurrent facility failures (Big Data analytics, CDN);
- **Amazon S3 Standard-Infrequent Access (IA)**: for data that is less frequently accessed, but requires rapid access. Similar to Amazon S3 Standard but has a lower storage price and higher retrieval price. Lower cost than S3 Standard (Disaster Recovery), backups; 
- **Amazon S3 One Zone-Infrequent Access**: data will be lost when AZ is destroyed (storing secondary backup copies, data you can recreate);
- **Amazon S3 Glacier Instant Retrieval**: *(milisecond retrieval, but once a quarter, minimum storage duration 90 days)*. Low-cost object storage with pricing for storage and retrieval cost;
- **Amazon S3 Glacier Flexible Retrieval** *(minimum storage duration of 90 days)*:
    - Expedited *(1 to 5 minutes)*;
    - Standard *(3 to 5 minutes)*;
    - Bulk *(5 to 12 hours)* - free.
- **Amazon S3 Glacier Deep Archive**: minimum storage duration of 180 days:
    - Standard (12 hours);
    - Bulk (48 hours).
- **Amazon S3 Intelligent Tiering**: moves objects automatically between Access Tiers based on usage, small monthly monitoring and auto-tiering fee, but there are no retrieval charges. Ideal for data with unknown or changing access patterns. Requires a small monthly monitoring and automation fee per object.
    - Frequent Access tier (automatic): default tier;
    - Infrequent Access tier (automatic): object not accessed  for 30 days;
    - Archive Instant Access tier (automatic): objects not accessed for 90 days;
    - Archive Access tier (optional): configurable from 90 days to 700+ days;
    - Deep Archive Access tier (optional): configurable from 180 days to 700+ days.
*Start from Amazon S3 Standard / Stardard-IA.* <br>
*Without details on usage the most cost effective type is Amazon S3 Intelligent Tiering.* <br>
*Amazon S3 Glacier is about long retrievals.* <br>


### **<div align="left">AWS Snow Family:</div>**
**AWS Snow Family:** highly-secure, portable devices to collect and process data at the edge, and migrate data into and out of AWS.
Trying to resolve challenges like:
- Limited connectivity;
- Limited bandwidth;
- High network cost (limited bandwidth, bad connection stability).


**Data migration and Edge computing**:
- **Snowcone**: 2 CPUs, 4 GiB RAM and 8 TB of HDD Storage or 14 TB of SSD Storage;
- **Snowball Edge**:
    - Snowball Edge Storage Optimized: 40 vCPUs, 80 GiB RAM, 80 TB of HDD capacity;
    - Snowball Edge Compute Optimized: 104 vCPUs, 416 GiB RAM, 42 TB of HDD or 28 TB NVMe capacity.
- **Snowmobile**: 1 EB (1000 PB) total and each Snowmobile 100 PB of capacity (better than Snowball if transfering more than 10 PB of data);


**AWS OpsHub** is a software to manage Snow Family Devices.


## **<div align="left">Databases:</div>**
**RDS (Relational Database Service)** is a distributed relational database service (SQL).<br> 
- Postgres;
- MySQL;
- MariaDB;
- Oracle;
- Microsoft SQL Server;
- IBM DB2;
- Aurora (AWS Proprietary DB).


**Advantages of RDS vs EC2 based DB**:
- Automated provisioning, OS patching;
- Continuous backups and restore to specific timestamp (Point in Time Restore);
- Monitoring dashboards;
- Read replicas for improved read performance;
- Multi AZ setup for DR (Disaster Recovery);
- Maintenance windows for upgrades;
- Scaling capability (vertical and horizontal);
- Storage backed by EBS.


**Amazon Aurora** is "AWS cloud optimized" (x5 performance over MySQL and x3 performance over Postgres) an enterprise-class relational database, proprietary technology from AWS (not open source). Automatically growing storage. Costs more than RDS on 20%, but it's more efficient, Amazon Aurora helps to reduce your database costs by reducing unnecessary input/output (I/O) operations, while ensuring that your database resources remain reliable and available. <br>
*Amazon Aurora replicates six copies of your data across three Availability Zones and continuously backs up your data to Amazon S3.*


**Amazon ElastiCache** managed Redis or Memcached in-memory databases with high performance and low latency to reduce load off databases for read intensive workloads.


**DynamoDB** fully managed highly available (with replication across 3 AZ), NoSQL (key/value) database that scales to massive workloads and single-digit millisecond latency. <br>
**DynamoDB Accelerator - DAX** fully managed in-memory chage for DynamoDB (x10 perfomance improvement). Like ElastiCache, but only for DynamoDB.


**RedShift** is a data warehousing service that you can use for big data analytics (based on PostgreSQL). It offers the ability to collect data from many sources and helps you to understand relationships and trends across your data.


**Amazon Elastic MapReduce (EMR)** helps create Hadoop clusters (Big Data) to analyze and process vast amount of data (big data, machine learning, web indexing). Can be made of hundreds of EC2 instances (integrated with Spot Instances).


**Amazon Athena** serverless SQL based service to analyze data stored in Amazon S3 (business intelligence, analytics, VPC Flow Logs, ELB Logs, CloudTrail, etc).


**Amazon QuickSight** serverless machine learing-powered business intelligence service to create interactive dashboards (business analytics, building visualizations, perform ad-hoc analysis). Integrated with RDS, Aurora, Athena, RedShift, S3.


**DocumentDB** is a document database (NoSQL) service that supports MongoDB workloads, proprietary fully managed and highly available across 3 AZ. Automatically grows and scales to workloads with millions of requests per second.


**Amazon Neptune** is a fully managed graph database. Usually for graph data sets like social network, knowledge graphs (Wikipedia), recomendation engines and fraud detection. Highly available across 3 AZ, with up to 15 replicas.


**Amazon Timestream** fully managed, fast, scalable, serverlesss time series database. Built-in time series analytics functions (helps you identify patterns in your data in near real-time).


**Amazon QLDB (Quantum Ledger Database)** is a fully managed, serverless, highly available book recording financial transactions. (Unlike Amazon Managed Blockchain there is no decentralization component).


**Amazon Managed Blockchain** is managed blockchain service to join public blockchain networks or create your own scalable private network, without the need for a trusted, central authority. Compatible with Hyperledger Fabric and Etherium.


**AWS Glue** fully serverless managed extract, transform and load (ETL) service to prepare and transform data for analytics (can be used by Athena, RedShift, EMR).


## **<div align="left">Amazon Elastic Container Service (ECS):</div>**
**Amazon Elastic Container Service (Amazon ECS)** is a fully managed container orchestration service that helps you easily deploy, manage, and scale containerized applications. Amazon ESC intergrated with Application Load Balancer (ALB). 


**Types of provisioning**:
- **EC2 instances based** provisioning, where customer is maintaining the infrastructure and Amazon managing starting and stopping of containers.
- **Fargate**: serverless managed service to run containes just based on the required CPU/RAM.
*Fargate works with both Amazon ECS and Amazon EKS.*


**Amazon Elastic Container Registry (ECR)** is a public or private registry to store container images, so they can be run by ECS.


**ECS task execution role** is capabilities of ECS agent (and container instance), e.g:
- Pulling a container image from Amazon ECR;
- Using the awslogs log driver;
**ECS task role** is specific capabilities within the task itself, e.g:
- When your actual code runs.


## **<div align="left">Amazon Elastic Kubernetes Service (EKS):</div>**
**Amazon Elastic Kubernetes Service (Amazon EKS)** is a fully managed service that you can use to run Kubernetes on AWS. 


## **<div align="left">Amazon Lambda and Batch:</div>**
**Amazon Lambda** is serverless, autoscaled, event-driven service to run on-demand virual functions. Supports many programming languages.


**Amazon API Gateway** fully managed, serverless and scalable service for developers to easily create, publish, maintain and monitor APIs. Support RESTful APIs and WebSocket APIs. 


**AWS Batch** fully managed batch processing at any scale. Batch will dynaicatlly launch EC2 instances or Spot Instances. Batch jobs are defined as Docker images and run on ECS.
AWS Batch has no time limits unlike AWS Lambda, not limited by runtimes as long as it packaged in Docker container and relies on EBS or instance store for disk space.


## **<div align="left">Amazon Lightsail:</div>**
**Amazon Lightsail** simplified alternative version of AWS services, used for simple web applications (has templates for LAMP, Nginx, MEAN, Node.js..), websites (templates for Wordpress, Magento, Plesk, Joomla), Dev/Test environment. Has high availability but no auto-scaling, limited AWS integrations.


## **<div align="center">Pricing:</div>**
**Pricing Models in AWS:**
- **Pay as you go**: pay for what you use, remain agile, responsive, meet scale demands;
- **Save when you reserve**: minimize risks, predictably manage budgets, comply with long-terms requirements;
- **Pay less by using more**: volume-based discounts;
- **Pay less as AWS grows**: get discount for long-term loyalty to AWS.


**Examples of spending categories:** 
- **Compute**: Pay for compute time;
- **Storage**: Pay for data stored in the Cloud;
- **Data transers OUT of the Cloud**: Data transfer IN is free.


**Free services & free tier in AWS:**
- IAM;
- VPC;
- Consolidated Billing;
- Elastic Beanstalk (pay for the resources created);
- CloudFormation (pay for the resources created);
- Auto Scaling Groups (pay for the resources created);
- EC2 Image Builder (pay for the resources created);
- ECS with EC2 Launch Type Model (pay for the resources created);
- Free Tier:
    - EC2 (t2.micro);
    - S3, EBS, ELB, AWS Data transfer.


**EC2 Instances Purchasing Options:**
- **On-Demand Instances:** *(Pay for what you use)* short-term and un-interrupted workload, no upfront payment - predictable pricing, highest cost - pay by second;
- **Reserved instances** *(up to 75% discount, with 1 or 3 years)*:
    - **Standard Reserved Instances** *(fixed Instance Type, Region, Tenancy, OS)*: long steady-state workloads (like database);
    - **Convertible Reserved Instances**: *(mutable Instance type, Region, Tenancy, OS)*: long workloads with flexible instances;
- **Saving Plans** *(up to 72%, with 1 or 3 years)*: commited to an amount of usage (ex. 10$/hour) of Instance Type in Region, long workload;
- **Compute Saving Plan** *(up to 66% discount)*: regardless of Instance Type, Region, Size, OS, Tenancy, Compute options;
- **Spot Instances** *(up to 90% discount)*: cost-efficient short-term workload, resilient to failure and less reliable instances *(randomly dies with 2 min warning)*;
- **Dedicated Host**: book an entire physical server, control instance placement to address compliance requirements and use your existing server-bound software licenses;
    - **On-demand** *(most expensive option)*: pay per second for active Dedicated Host;
    - **Reserved** *(with 1 or 3 years)*: with No Upfront, Partial Upfront and All Upfront payment options.
- **Dedicated Instances**: no other customers will share dedicated to you hardware. But there is no control over hardware placement;
- **Capacity Reservations**: reserve capacity (Instance Type, Tenancy, and OS) in a specific AZ for any duration.


**EC2 Image Builder** only pay for the underlying resources.


**EBS Storage billed:**
- For all provisioned capacity;
- For IOPS if SSD and for number of request if HDD;
- Snapshots added data coast per GB per month;
- Outbound traffic are tiered for volume discounts.


**EFS (Elastic File System)**:
- Pay per use;
- Storage class;
- Lifecycle rules.


**S3 Pricing**:
- Storage class: S3 Standard, S3 Glacier Deep Archive, etc;
- Number and size of objects: Price can be tiered (based on volume);
- Number and type of requests;
- Data transfer OUT of the S3 region;
- S3 Transfer Acceleration;
- Lifecycle transitions.


**ECS pricing**:
- EC2 Launch Type Model: No additional fees, you pay AWS resources stored and created in your application;
- Fargate Launch Type Model: Pay for vCPU and memory resources allocated to your applications in your containers.


**Lambda pricing**:
- Pay per call;
- Performance (RAM);
- Pay per duration.


**Snowball Family Pricing**:
AWS Snowball offers significantly discounted pricing (up to 62%) for 1-year usage and 3-year usage commitments for Edge compute use cases.


**Database pricing - RDS:**
- Per hour billing;
- Database characteristics: Engine, Size, Memory class;
- Purchase types: On-demand, Reserved instances;
- Backup storage: no additional charge for backup storage up to 100% of your total database storage for a region.
- Additional storage (per GB per month);
- Number of input and output requests per month;
- Deployment type (storage and I/O are variable): Single or Multiple AZ;
- Data transfer:
    - Outbound traffic are tiered for volume discounts;
    - Inbound is free.


**CloudFront pricing:**
- Pricing is different across different geographic regions;
- Aggregated for each edge location, then applied to your bill;
- Data Transfer Out (volume discount);
- Number of HTTP/HTTPS requests.


## **<div align="left">Billing and Costing Tools:</div>**
**Pricing Calculator**: estimate the cost for your solution architecture.


**AWS Billing Dashboard**: home page for an overview of your AWS cloud financial management data and to help you make faster and more informed decisions.
**AWS Free Tier Dashboard**: tracking AWS Free Tier usage.


**Cost Allocation Tags**: use cost allocation tags to track AWS costs on a detailed level.


**Tagging and Resource Groups**:
- Tags are used for organizing resources;
- Resource Groups are used for creating, maintaining and viewing a collection of resources that share common tags.


**Cost and Usage Reports**: lists AWS usage for each service category used by an account and its IAM users in hourly or daily line items, as well as any tags that customer activated for cost allocation purposes, including additional metadata about AWS services, pricing and reservations. 


**Cost Explorer**: an easy-to-use interface that lets you visualize, understand, and manage your AWS costs and usage over time. Create custom reports that analyze cost and usage data. High level analytics of total costs and usage across all accounts. Alternative Saving Plans suggestions to lower costs. <br>
*Forecast usage up to 12 months based on previous usage.* <br>
*Allows under/high-utilization of EC2 Instances or EBS Storage.*


**Billing Alarms in CloudWatch**: intended simple alarm  for actual cost, not for projected costs, based on billing data metric stored in CloudWatch.


**AWS Budgets**: set custom budgets to track your costs and usage, and respond quickly to alerts received from email or SNS notifications if you exceed your threshold.
- Usage;
- Cost;
- Reservation;
- Saving Plans.


**AWS Cost Anomaly Detection**: continuously monitor your cost and usage using ML to detect unusual spends. It learns your unique, historic spend patterns to detect one-time cost spike and/or continuous cost increase. Sends you the anomaly detection report with root-cause analysis.


**AWS Service Quotas**: notifies you when you're close to a service quota value threshold. Create CloudWatch Alarms. Request a quota increase from AWS Service Quotas or shutdown resources before limit is reached.


**AWS Trusted Advisor**: analyze your AWS accounts and provides recommendation on 6 categories:
- Cost optimization: under-utilization;
- Performance;
- Security;
- Fault tolerance;
- Service limits;
- Operational Excellence.
- Full set of Checks *(Only for Business & Enterprise Support plan)*;
- Programmatic Access using AWS Support API *(Business & Enterprise Support plan)*.


**AWS Compute Optimizer**: uses ML to analyze existing resources' configurations and their utilization CloudWatch metrics, helps to choose optimal configurations and right-size your workloads (over/under provisioned). Supports: EC2 Instances, EC2 Auto Scaling Groups, EBS volumes, Lambda functions. Recomendations can be exported to S3.


## **<div align="center">AWS Support Plans Pricing:</div>**
**AWS Basic Support (free)**:
- Customer Service & Communities: 24/7 access to customer service, documentation and support forums;
- AWS Trusted Advisor: Access to the 7 core Trusted Advisor checks and guidance to provision your resources following best practices to increase performance and improve security;
- AWS Person Health Dashboard: A personalized view of the health of AWS services, and alerts when your resources are impacted.


**AWS Developer Support Plan**:
- All Basic Support Plans features included;
- Business hours email access to Cloud Support Associates;
- Unlimited cases (1 primary contact);
- Case severity / response times:
    - General guidance: <24 business hours;
    - System impaired: <12 business hours.


**AWS Business Support Plan (24/7)**:
- Intended to be used on production workloads;
- Trusted Advisor: full set of checks + API access;
- 24/7 phone, email and chat access to Cloud Support Engineers;
- Unlimited cases / unlimited contacts;
- Access to Infrastructure Event Management for additional fee;
- Case severity / response times:
    - General guidance: <24 business hours;
    - System impaired: <12 business hours;
    - Production system impaired: <4 hours;
    - Production system down: <1 hour.


**AWS Enterprise On-Ramp Support Plan (24/7)**:
- Intended to be used if you have production or business crtical workloads;
- All of Business Suport Plan features included;
- Access to a pool of Technical Account Managers (TAM);
- Concierge Support Team (for billing and account best practices);
- Infrastructure Event Management, Well-Archiected & Operations Reviews;
- Case severity / response times:
    - General guidance: <24 business hours;
    - System impaired: <12 business hours;
    - Production system impaired: <4 hours;
    - Production system down: <1 hour;
    - Business-critical system down: <30 minutes.


**AWS Enterprise Support Plan (24/7)**:
- Intended to be used if you have mission critical workloads:
- All of Business Support Plan features included;
- Access to designated Technical Account Manager (TAM);
- Concierge Support Team (for billing and account best practices);
- Infrastructure Event Management, Well-Architected & Operations Reviews;
- Case severity / response times:
    - General guidance: <24 business hours;
    - System impaired: <12 business hours;
    - Production system impaired: <4 hours;
    - Production system down: <1 hour.
    - Business-critical system down: <15 minutes.


## **<div align="left">Scalability and High Availability:</div>**
**Scalability** means that an application or system can handle greater loads by adapting.
- **Vertical scalability:** means increasing the size of the instance, common for non distributed systems (databases). There's usually a limit to how much you can vertically scale (hardware limit);
- **Horizontal scalability:** increasing the numbers of instances or systems for you application, implies disctributed systems (modern web applications). 


**High Availability:** survivalability of a data center loss (disaster). Running application or system in at lease two AZs. <br>
*Fault-tolerant systems emphasize maintaining continuous operation during unexpected failures, while high-availability infrastructures prioritize keeping services up and running despite scheduled maintenance or potential bottlenecks.*


**Scalability vs Elasticity vs Agility:**
- **Scalability:** ability to accommodate a larger load by making the hardware stronger (scale up), or by adding nodes (scale out);
- **Elasticity:** once a system is scalable, elasticity means that there will be some "auto-scale" so that the system can scale based on the load (pay-per-use, match demand, optimize costs);
- **Agility:** reduced time to make IT resources available.


**Elastic Load Balancer (ELB)** - service that automatically distributes incoming application traffic across multiple resources, such as Amazon EC2 instances.
- Spread load across multiple downstream instances;
- Expose a single point of access (DNS) to your application;
- Seamlessly handle failure of downstream instance;
- Do regular health checks to instances;
- High availability across zones.


**Types of load balancers**:
- **Application Load Balancer** (HTTP/HTTPS only) - Layer 7;
- **Network Load Balancer** (ultra-hight performance, allows for TCP) - Layer 4;
- **Gateway Load Balancer** - Layer 3;


**Auto Scaling Group (ASG):** managed service that ensures that there is an optimal capacity is in use.
- Scale out (register new instances to a load balancer) to match increased load;
- Scale in (remove EC2 instances) to match a decreased load;
- Replace unhealthy instances.


**Auto Scaling Groups - Capacity characteristics**:
- Minimum capacity (number of instances);
- Desired capacity (number of instances);
- Scale as needed (required by demand capacity);
- Maximum capacity (number of instances).


**Auto Scaling Groups - Scaling Strategies:**
**Manual Scaling:** Update the size of an ASG manually;
**Dynamic Scaling:** Respond to changing demand:
    - **Simple / Step Scaling:**
        - When a CloudWatch alarm is triggered (example CPU > 70%), then add 2 units;
        - When a CloudWatch alarm is triggered (example CPU <30%), then remove 1 unit;
    - **Target Tracking Scaling:**
        - Goal is to stay at average CPU usage at around 40%;
    - **Scheduling Scaling:**
        - Anticipate a scaling based on known usage patterns;
    - **Predictive Scaling:**
        - Uses Machine Learning to predict future traffic ahead of time;


## **<div align="left">Decoupling Services (Microservices Approach):</div>**
**Amazon SQS (Simple Queue Service)** is fully managed, serverless messaging service that is used to decouple aplications. Send, store, and receive messages between software components, without losing messages or requiring other services to be available. In Amazon SQS, an application sends messages into a queue. A user or service retrieves a message from the queue, processes it, and then deletes it from the queue.


**Amazon SNS** is a fully managed, serverless, publish/subscribe notification service. Using Amazon SNS topics, a publisher publishes messages to subscribers:
- Web servers;
- Email addresses;
- AWS Lambda functions;
- Amazon SQS;
- Amazon Kinesis Data Firehose.
*And more.*


**Amazon MQ** is a managed message broker service for RabbitMQ and ActiveMQ.


**Amazon Kinesis** is a managed service to collect, process and analyze real-time streaming data at any scale. 
- Kinesis Data Streams: low latency streaming to ingest data at scale from hundreds of thousands of source;
- Kinesis Data Firehose: load stream into S3, Redshift, ElasticSearch, etc;
- Kinesis Data Analytics: perform real-time analytics on streams using SQL;
- Kinesis Video Streams: monitor real-time video streams for analytics or ML.


## **<div align="left">Monitoring:</div>**
**Amazon CloudWatch** is a service that monitors applications, responds to performance changes, optimizes resource use, and provides insights into operational health. By collecting data across AWS resources, CloudWatch gives visibility into system-wide performance and allows users to set alarms, automatically react to changes, and gain a unified view of operational health.


**Important metrics**:
- **EC2 instances**: CPU Utilization, Status Checks, Network (not RAM);
    - Default metric every 5 minutes;
    - Option for Detailed Monitoring (every 1 minute) costs additional money.
- **EBS volumes**: Disk Read/Writes;
- **S3 buckets**: BucketSizeBytes, NumberOfObjects, AllRequests;
- **Billing**: Total Estimated Charge (only us-east-1);
- **Service Limits**: how much you've been using a service API;
- **Custom metrics**: push your own metrics.


**Amazon CloudWatch Alarms** are used to trigger notifications for any metric.
- **Auto scaling**: increase or decrease EC2 instances "desired" count;
- **EC2 Actions**: stop, terminate, reboot or recover an EC2 instance;
- **SNS notifications**: send a notification into n SNS topic.


**Amazon CloudWatch Logs**:
- **Elastic Beanstalk**: collection of logs from application;
- **ECS**: collection from container;
- **AWS Lambda**: collections from function logs;
- **CloudTrail** based of filter;
- **CloudWatch lof agents**: on EC2 machines or ion-premise servers;
- **Route53**: log DNS queries.


**Amazon EventBridge** is a serverless event bus that ingests data from your own apps, SaaS apps, and AWS services and routes that data to targets.
- Schedule: Cron jobs (scheduled scripts);
- Event Pattern: Event rules to react to a service doing something;
- Trigger Lambda fuctions, send SQS/SNS messages.
*EventBridge Scheduler is ideal for simple scheduling tasks, providing a straightforward interface and cost-effective solution for triggering events at specific times.*


**Amazon CloudTrail** is an AWS service that helps you enable operational and risk auditing, governance, and compliance of your AWS account. Actions taken by a user, role, or an AWS service are recorded as events in CloudTrail. Get an history of events / API calls made in the AWS Management Console, AWS Command Line Interface, and AWS SDKs and APIs. Can put logs from CloudTrail into CloudWatch Logs or S3.
*Audit of all users' events and activities.*


**AWS X-Ray** provides a complete view of requests as they travel through your application and filters visual data across payloads, functions, traces, services, APIs, and more with no-code and low-code motions.
- Analyze and debug applications Receive trace data from your simple and complex applications, whether they are in development or production;
- Generate a detailed service map: Compile data from your AWS resources to determine bottlenecks in your cloud architecture and improve application performance;
- View performance analytics: Compare trace sets with different conditions for root cause analysis purposes;
- Audit your data securely: Configure X-Ray to meet your security and compliance objectives.
*Aimed on analyzing decentralyzed application*.


**Amazon CodeGuru** is a static application security testing (SAST) tool that combines machine learning (ML) and automated reasoning to identify vulnerabilities in your code, provide recommendations on how to fix the identified vulnerabilities, and track the status of the vulnerabilities until closure.
- **Amazon CodeGuru Reviewer**: automated code reviews for static code analysis (development).
- **Amazon CodeGuru Profiler**: helping to understand the runtime behavior of applications, identify and remove code inefficiencies, improve performance, and significantly decrease compute costs. 


**AWS Health Dashboard** view the overall status and health of AWS services.
**AWS Health Dashboard - Your Account** provides alerts and remediation guidance when AWS is experiencng events that may impact you.


## **<div align="center">Security:</div>**
### **<div align="left">AWS Security Groups (SG):</div>**
Security Group (Firewall) controls how traffic is allowed into or out of EC2 Instances or other Security Groups, 
Can be attached to multiple instances. Locked down to a region/VPC combination. <br>
*Security Group by default denies every inbound traffic and contain only allow rules. All outbound traffic is authorised.*


**Security Group Rules regulate**:
- Access to Ports;
- Authorised IP ranges (IPv4&IPv6);
- Control of inbound and outbound network from one instance to another.


**Best practices**:
- Maintain separate Security Group for SSH access;


**Troubleshooting**:
- If application is not accessible (time out), then it's a security group issue;
- If application give a "connection refused" error, then it's an application error or it's not launched.


### **<div align="left">Amazon S3 - Security:</div>**
- **User-Based**:
    - IAM Policies: which API calls should be allowed for a specific user from IAM;
- **Resource-Based**:
    - Bucket Policies;
    - Object Access Control List (ACL);
    - Bucket Access Control List (ACL).
*An IAM principal can access an S3 object if the user IAM permissions ALLOW it OR the resource policy ALLOWS it. There is no explicit DENY needed. Only Block all public access settings, to prevent data leaks.*


**IAM Access Analyzer for S3**:
- Ensures that only intended people have access to your S3 buckets;
- Publicly accessible bucket, bucket shared with other AWS account;
- Evaluates S3 Bucket Policies, S3 ACLs, S3 Access Point Policies.


### **<div align="left">Network Protection:</div>**
**DDoS Protection on AWS**:
- **AWS Shield Standard (free)**: protects against DDoS attack (SYN/UDP Floods, Reflection attacks and other layer 3/4 attacks) for your website and applications, for all customers at no additional costs.
- **AWS Shield Advanced (additional cost)**: 24/7 premium DDoS protection from more sophisticated attack on AWS Services. Protects against higher fees during usage spikes due to DDoS.


**AWS WAF**: filter specific requests based on rules and protects web application from common web exploits (Layer 7). Deploy on Application Load Balancer, API Gateway and CloudFront.


**Define Web ACL (Web Access Controll List)**:
- Rules can include IP address, HTTP headers, HTTP body or URI strings;
- Protects from common attack: SQL injection and Cross-Site Scripting (XSS);
- Size contrains, geo-match (block countries);
- Rate-based rules (to count occurences of events) for DDoS protection.


**AWS Network Firewall** protect entire Amazon VPC (from layer 3 to layer 7).
**Inspect directions**:
- VPC to VPC traffic;
- Outbound/inbound internet traffic;
- To/from Direct Connect & Site-to-Site VPN.


**AWS Firewall Manager** manages security rules in all accounts of an AWS Organization. Rules are applied to new resources as they are created (good for compliance) across all and future accounts in your Organization.


**Security policies**:
- VPC Security Groups for EC2, Application Load Balancer, etc;
- AWS WAF Rules;
- AWS Shield Advanced;
- AWS Network Firewall.


### **<div align="left">Penetration Testing and Abusing activities on AWS Cloud:</div>**
**AWS Acceptable use policy:**
- No illigal, Harmful, or Offensive Use or Content;
- No Security Violations;
- No Network Abuse;
- No E-mail or Other Message Abuse.


**Eight servics that are allowed without prior approval from AWS to carry out security assessments or penetration tests**:
- Amazon EC2 instances, NAT Gateways and Elastic Load Balancers;
- Amazon RDS;
- Amazon CloudFront;
- Amazon Aurora;
- Amazon API Gateways;
- AWS Lambda and Lambda Edge functions;
- Amazon Lightsail resources;
- Amazon Elastic Beanstalk environment.


**Prohibited Actiities**:
- DNS zone walking via Amazon Route 53 Hosted Zones;
- Denial of Service (DoS), Distributed Denial of Service (DDoS), Simulated DoS/DDoS;
- Port flooding;
- Protocol flooding;
- Request flooding (login request flooding, API request flooding).


**AWS Abuse** report suspected AWS resources used for abusive or illegal purposes.


**Abusive & prohibited behaviors are**:
- **Spam**: recieving undesired emails from AWS-owned IP address, websites & forums spammed by AWS resources;
- **Port scanning**: sending packets to ports to discover the unsecured ones;
- **DoS/DDoS attacks**: AWS-owned IP addresses attempting to overwhelm or crash service;
- **Intrusion attempts**: logging in on your resources;
- **Hosting objectionable or copyrighted content**: distributing illigal or copyrighted content without consent;
- **Disctributing malware**: AWS resources distributing software to harm computers.


### **<div align="left">Encryption Management:</div>**
**AWS KMS (Key Management Service)** service that helps manage the ecryption keys.


**Encryption Opt-in**:
- EBS volumes: encrypt volumes;
- S3 buckets: Server-side encryption of objects;
- Redshift database: encryption of data;
- RDS database: encryption of data;
- EFS drives: encryption of data.


**Encryption Automatically enabled**:
- CloudTrail Logs;
- S3 Glacier;
- Storage Gateway.


**AWS CloudHSM (Cloud Hardware Security Module)** provisioning encryption hardware. Customer manages all ecryption keys.


**Type of KMS Keys (based on creating, managing, using rotaion policies)**:
- Customer Managed Keys;
- AWS Managed key;
- AWS Owned Keys;
- CloudHSM Keys.


**AWS Certificate Manager (ACM)** is a managed service to provision, manage, and deploy public and private SSL/TLS certificates with AWS services and internal connected resources. Intergrated with Elastic Load Balancer, CloudFront Distributions, APIs on API Gateway.


**AWS Secrets Manager** helps you manage, retrieve, and rotate database credentials, API keys, and other secrets throughout their lifecycles. Integrated with AWS Lambda, AWS RDS (MySQL, PostgreSQL, Aurora).


**Rotation sercrets** is the process of periodically updating a secret. When you rotate a secret, you update the credentials in both the secret and the database or service. In Secrets Manager, you can set up automatic rotation for your secrets.
- **Managed rotation**: for most managed secrets, you use managed rotation, where the service configures and manages rotation for you. Managed rotation doesn't use a Lambda function;
- **Rotation by Lambda function**: for other types of secrets, Secrets Manager rotation uses a Lambda function to update the secret and the database or service.


**AWS Systems Manager Parameter Store** provides secure, hierarchical storage for configuration data management and secrets management. You can store data such as passwords, database strings, and license codes as parameter values. <br>
*Parameter Store doesn't provide automatic rotation services for stored secrets.*


### **<div align="left">Other Security Tools:</div>**
**Amazon GuardDuty** intelligent (uses Machine Learning) threat discovery to protect your AWS account.


**Input data includes**:
- CloudTrail Events Logs: unusual API calls, unauthorized deployments:
    - CloudTrail Management Events: create VPC subnet, create trail, etc;
    - CloudTrail S3 Data Events: get object, list objects, delete object, etc;
- VPC Flow Logs: usual internal traffic, unusual IP address;
- DNS Logs: compromised EC2 instances sending encoded data within DNS queries;
- Optional Features: EKS Audit Logs, RDS & Aurora, EBS, Lambda, S3 Data Events, etc
- Can protect against CryptoCurrency attacks.


**Amazon Inspector** automatically discovers workloads, such as Amazon EC2 instances, containers, and Lambda functions, and scans them for software vulnerabilities and unintended network exposure.
- **For EC2 instances**:
    - Leveraging the AWS System Manager (SSM) agent;
    - Analyze againt unintended network accessibility;
    - Analyze the running OS against known vulnerabilities;
- **For Container Images push to Amazon ECR**:
    - Assessment of Container Images as they are pushed;
- **For Lambda Functions**:
    - Identifies software vulnerabilities in fucntion code and packages dependencies;
    - Assessment of functions as they are deplayed.
*Reporting & integraion with AWS Security Hub. Send findings to Amazon Event Bridge.*


**AWS Config** is a config tool that helps you assess, audit, and evaluate the configurations and relationships of your resources. Possibility of storing the configuration data into S3 (analyzed by Athena) and recieving alerts (SNS notifications) for any changes. Per-region service, but can be aggregated across regions and accounts.
- "Is there unresctricted SSH access to my security groups?";
- "Do my buckets have any public access?";
- "How has my ALB configuration changed over time?.


**AWS Macie** is a fully managed data security and data privacy service that uses machine learning and pattern matching to discover and protect your sensitive data in AWS. Macie helps identify and alert you to sensitive data, such as personally identifiable information (PII). 


**AWS Detective** analyzes, investigates and quickly identifies the root cause of security issues or suspicious activities (using ML and graphs). Automatically collects and processes events from VPC Flow Logs, CloudTrail, GuardDuty and create unified view.


**AWS Security Hub** central security tool to manage security across several AWS accounts and automate security checks. Integrated dashboards showing current security and compliance status to quickly take actions. 
*Not enabled by default*


**Autmatically aggregates alerts**:
- AWS Config;
- AWS GuardDuty;
- AWS Inspector;
- AWS Macie;
- IAM Access Analyzer;
- AWS Systems Manager;
- AWS Firewall Manager;
- AWS Health;
- AWS Partner Network Solutions.
*All discovered data can be used by AWS EventBridge, AWS Security Hub Findings, Amazon Detective.*


### **<div align="center">Security Compliances and Reports:</div>**
**AWS Artifact** (not really a service) portal that provides customers with on-demand access to AWS compliance documentation and AWS agreements. Can be used to support internal audit or compliance.
- Artifact Reports: Allows you to download AWS security and compliance documents from third-party auditors (AWS ISO, Payment Card industry and System and Organization Control);
- Artifact Agreements: Allows you to review, accept and track the status of AWS agreements (as Business Associate Addendum, Health Insurance Portability and Acountability Act).


**IAM Access Analyzer**: finds out which resources are shared externally of defined Zone of Trust (AWS account or AWS Organization):
- S3 Buckets;
- IAM Roles;
- KMS Keys;
- Lambda Functions and Layers;
- SQS queues;
- Secrets Manager Secrets.


## **<div align="left">Deployment (IaaS) and software development (CI/CD):</div>**
**AWS Cloudformation** is a declarative way of outlining and creating your AWS Infrastructure, for anuy resources, in the right order and with exact configuration that you specify.
- Infrastructure as a code (IaaS): no resources are manually created, changes to the infrastructure are reviewed through code;
- Cost: based on CloudFormation template there is estimated cost. Saving strategy: automation deletion and recreation of templates based on scheduling;
- Productivity: Ability to destroy and re-create an infrastructure  on the cloud on the fly. Declarative programming (no need to figure out ordering and orchestration);
- Don't re-invent the wheel: :everage existing templates on the web, leverage the documentation.


**AWS Application Composer**: visually design and build serverless applications quickly on AWS. Deploy AWS infrastructure code without needing to be an expert in AWS. Configure how your resources interat with each other. Ability to import existing CloudFormation / SAM templates to visualize them.
*Help to visualize, build, and deploy modern applications from all AWS services that are supported by AWS CloudFormation.*


**AWS Cloud Development Kit (CDK)**  accelerates cloud development using common programming languages to model your applications, to deplay infrastructure and applicationg runtime code together. 


**AWS Elastic Beanstalk** is a managed service of Platform as a Service (PaaS), developer centric view of deploying an application on AWS (using EC2, ASG, ELB, RDS and etc). Instance configuration, OS handling, deployment strategy, capacity provisioning, load balancing and auto-scaling, application health-monitoring & responsiveness, everything except the actual application code is responsibility of AWS Elastic Beanstalk. <br>
*Elastic Beanstalk automatically handles capacity provisioning, load balancing, autoscaling and application health monitoring.*


**AWS CodeDeploy** is a fully managed deployment service that automates software deployments to various compute services, such as Amazon Elastic Compute Cloud (EC2), Amazon Elastic Container Service (ECS), AWS Lambda, and your on-premises servers. Use CodeDeploy to automate software deployments, eliminating the need for error-prone manual operations.


**AWS CodeCommit** is a fully managed, scalable and highly available code repository, using Git technology. Collaborate with others on code. Code changes are automatically versioned.


**AWS CodeBuild** is a fully managed, serverless, scalable & highly availble code building service in the cloud. Compiles source code, run tests and produces packages that are ready to be deployed.


**AWS CodePipeline** is a fully managed continuous delivery service that helps you automate your release pipelines for fast and reliable application and infrastructure updates. Compatible with CodeCommit, CodeBuild, CodeDeploy, Elastic Beanstalk, CloudFormation, GitHub, etc.


**AWS CodeArtifact** is a secure, scalable, and cost-effective package management for software development.


**AWS CodeStar** is a unified UI to easily manage software develompent activities in one place.


**AWS Cloud9** is a cloud IDE (Intergrated Development Environment) for writing, running and debugging code.


**AWS Step Functions**: serverless visual workflow to archestrate Labmda functions. Sequence, parallel, conditions, timeouts, error handling, human approval feature etc. Integrates with EC2, ECS, On-premises servers, API Gatewat, SQS queues, etc. <br>
*AWS Step Functions excel in complex workflow orchestration scenarios, offering advanced features such as state management, error handling, and parallel execution.*


**AWS Amplify**: a set of tools and services that helps you develop and deploy scalable full stack web and mobile applications. Authentication, Storage, API (REST, GraphQL), CI/CD, PubSub, Analytics, AI/ML Predictions, Monitoring, Source Code from AWS, GitHub, etc. <br>
*Amplify has serverless architecture simplifies maintenance and scales automatically. There is no need to provision or manage EC2 instances. Lambda and API Gateway handle availability and response to traffic spikes automatically. Upload code and let Amplify handle deploying and running it.*


**AWS Device Farm**: fully-managed service that tests your web and mobile apps against desktop browsers, real mobile devices and tablets. Run tests concurrently on multiple devices. Ability to configure device ettings (GPS, language, WiFi, Bluetooth, etc).


**AWS Systems Manager (SSM)** is a hybrid AWS service to get operational insights about the state of your infrastructure. Patching automation for enchanced compliance, runcommand across an entire fleet of servers.


## **<div align="left">Machine Learning (ML) Services:</div>**
**Amazon Rekognition** automates image recognition and video analysis for your applications without machine learning (ML) experience.
- Labeling;
- Content Moderation;
- Text Detection;
- Face Detection and Analysis (gender, age range, emotions);
- Face Search and Verifications;
- Celebrity Recognition;
- Pathing (for sport game analysis).


**Amazon Transcribe** automatically convert speech to text, using deep learning process called automatic speech recognition (ASR). Automatically removes Repsonally Identifiable Information (PII) using Redaction. Supports Automatic Language Identification for multi-lingual audio.


**Amazon Polly** turn text into lifelike speech using deep learning. Allows to create applications that talk.


**Amazon Translate** natural and accurate language translation.


**Amazon Lex** (technology that powers Alexa) easily add AI that understands intent, maintains context, and automates simple tasks across many languages to build chatbots, call center bots.


**Amazon Connect** is an omnichannel cloud contact center that helps companies provide superior customer service at a lower cost. Amazon Connect provides a seamless experience across voice and chat for customers and agents. 


**Amazon Comprehend** fully managed and serverless service for natural language processing (NLP), that uses machine learning to find insights and relationships in text: define language of the text, extract key pharses, understands emotions in the text, etc. Create and group articles by topics that Comprehend will uncover.


**Amazon SageMaker** is a fully managed service for developers / data scientists to build ML models.


**Amazon Forecast** is a fully managed service that uses ML to deliver highly accurate forecasts (product demand planning, financial planning, resource planning, etc).


**Amazon Kendra** is a fully managed document search service powered by ML. Extract answers from within a document (text, PDF, HTML, MS Office, etc) from different data sources (like Amazon S3, Google Drive, MS SharePoint, MS OneDrive, etc).


**Amazon Personalize** is a fully managed ML-service to build apps with real-time personalized recommendations (retail stores, media and entertainment, etc). 


**Amazon Textract** automatically extracts text, handwriting and data from any scanned docuemtns using AI and ML. Extract data from forms and tables: Financial Services (invoices, financial reports), Healthcare (medical records, insurance claims), Public Sector (tax forms, ID documents, passports).


## **<div align="left">Other AWS Services:</div>**
**Amazon WorkSpaces**: managed Desktop as a Service (DaaS) solution to easily provision Windows or Linux desktops. Cloud alternative to managing of on-premise Virtual Desktop Infrastructure (VDI). Scalable to thousands. Integrates with KMS. Pay-as-you-go pricing.


**Amazon AppStream 2.0**: desktop application streaming service. The application is delivered from within a web browser. Can be configured instance type per application type (CPU, RAM, GPU).


**AWS IoT Core**: serverless, secure & scalable to billions messages, service that allows easily connect IoT devices to AWS Cloud. 


**AWS AppSync**: store and sync data across mobile and web apps in real-time. Makes use of GraphQL (mobile technology from Facebook). Intergrations with DynamoDB / Lambda.


**AWS Ground Station**: is a fully managed service that lets you ontrol satellite communications, process data and scale your satellite operations (weather forecasting, surface imaging, videobroadcasting, etc). Provides global network of satellite ground stations nea AWS regions. Allows to download satellite data to AWS VPC within seconds and send it to S3 or EC2 Instances. 


**Amazon Pinpoint**: scalable two-way (outbound/inbound) marketing communications service (run campaigns by sending marketing, bulk, transactional SMS messages). Supports email, SMS, push, voice and in-app messaging. Ability to personalize messages with right content to customers. Possibility to recieve replies.
*Versus Amazon SNS or Amazon SES*: Amazon Pinpoint allows to create message templates, delivery schedules, highly-targeted segments and full campaigns.


**AWS Marketplace** digital catalog with thousands of software listings from independent software vendors (third-party).


## **<div align="left">Backup and Restore:</div>**
**AWS Backup**: fully-managed service to centrally manage and automate bacups across AWS services. On-demand and scheduled backups. Supports PITR (Point-in-time Recovery). Retention Periods, Lifecycle Management, Backup Policies. Cross-Region Backup. Cross-Account backup (using AWS Organization).


**AWS DataSync**: move large amount of data from on-primises to AWS. Can synchronize to: Amazon S3, Amazon EFS, Amazon FSx for Windows. Replication tasks can be scheduled hourly, daily, weekly. Replication tasks are incremental after the first full load.


**AWS Elastic Disaster recovery (DRS)**: quickly and easily recover your physical, virtual and cloud-based servers into AWS. Protect most critical databases, enterprise apps and data from ransomware attacks. Continuous block-level replication for you servers. 


**AWS Fault Injection Simulator (FIS)** is a fully manages service for running fault injection experiments on AWS workloads. Based on **Chaos Engineering**: stressing an application y creating desruptive events (sudden increase in CPU or memory)m observing how the system responds and impementing improvements. Helps uncover hidden bugs and performance bottlenecks. Supports: EC2, ECS, EKS, RDS. Use pre-built templates that generate the desired disruptions.


## **<div align="left">Migration Services:</div>**
**Cloud Migration Strategies: The 7Rs**:
- **Retire**:
    - Turn off things you don't need;
    - Helps with reducing the surface areas for attack (more security);
    - Save cost (up to 20%);
    - Focus your attention on resources that must be maintained.
- **Retain**:
    - Do nothing for now (it's still a decision t make in a Cloud Migration);
    - Security, data compliance, performance, unresolverd dependencies;
    - No business value to migrate, mainframe or mid-range and non-x86 Unix apps.
- **Relocate**:
    - Move apps from on-premises to its Cloud version;
    - Move EC2 instances to a different VPC, AWS account or AWS Region;
    - Transfer servers from VMware Software-defined Data Center (SSDC) to VMware Cloud on AWS.
- **Rehost ("lift and shift")**:
    - Simple migrations by re-hosting on AWS (applications, databases, etc);
    - Migrate machines (pysical, virtual, another Cloud) to AWS Cloud;
    - No cloud optimizations beging done, applications is migrated as is;
    - Could save as much as 30% on cost;
    - Migrate using AWS Application Migration Service.
- **Replatform ("lift and reshape")**:
    - Migrate your database to RDS;
    - Migrate your application to Elastic Beanstalk;
    - Not changing the core architecture, but leverage some Cloud optimizations;
    - Save time and money by moviing to a fully managed service or serverless.
- **Repurchase ("drop and shop")**:
    - Moving to a different product while moving to the Cloud;
    - Often you move to a SaaS platform;
    - Expensive in the short term, but quick to deploy;
    - CRM to Salesforce.com, HR to Workday, CMS to Drupal.
- **Refactor / Rearchitect**:
    - Reimagining how the application is architected using Cloud Native features;
    - Driven by the need of the business to add features and improve scalability, performance, security and agility;
    - Move from a monolithic application to micro-services;
    - Move an application to serverless architectures, use AWS S3.

 
**AWS Application Discovery Service**: service for gathering information about on-premises data centers.
- **Agentless Dicovery (AWS Agentless Discovery Connector)**: VM inventory, configuration and performance history such as CPU, memory and disk usage;
- **Agent-based Discovery (AWS Application Descovery Agent)**: system configuration, system performance, running processeses and details of the network connections between systems.


**AWS Application Migration Service (MGN)**: rehost ("lift-and-shift") solution which simplify migrating applications to AWS. Converts physical, virtual and cloud-based servers to run natively on AWS. Supports wide range of platforms, OSs and databases. Minimal downtime, reduced costs.
*Migration of on-premise Oracle DB to AWS EC2 Instance is an example of AWS Application Migration Service.*


**Amazon DMS (Database Migration Service)** quickly and securely migrate databases to AWS, while source database remains available during the migration. Supports homogeneous (Oracle to Oracle) and Heterogeneous migrations (Microsoft SQL Server to Aurora).


**AWS Migration Evaluator**: helps you build a data-driven business case for migration to AWS. Provides a clear baseline of what your organization is running today. Install Agentless Collector to conduct broad-based discovery. Take a snapshot of on-premises foot-print, server dependencies, etc. Analyze current state, define target state, then develop migration plan.


**AWS Migration Hub** central location to collect servers and applications inventory data for the assessment, planing and tracking of migrations to AWS. Helps accelerate you migration to AWS, automate lift-and-shift. **AWS MIgration Hub Orchestrator**: provides pre-built templates to save time effort migrating enterprise apps (SAP, Microsoft SQL Server, etc). Supports migrations status updates from Application Migration Service (MGN) and Database Migration Service (DMS).


### **<div align="left">AWS Cloud Practiotioner certificate:</div>**
https://www.w3schools.com/aws/aws_quiz.php

https://pages.awscloud.com/NAMER-partner-GC-Partner-Cert-Readiness-Cloud-Practitioner-2024-conf.html

https://www.udemy.com/course/aws-certified-cloud-practitioner-new/

https://media.datacumulus.com/aws-ccp/AWS%20Certified%20Cloud%20Practitioner%20Slides%20v28.pdf

>in progress..

https://www.examtopics.com/discussions/amazon/view/68991-exam-aws-certified-solutions-architect-associate-saa-c02/