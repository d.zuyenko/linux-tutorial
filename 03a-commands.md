

# <div align="center"> Built-in commands:</div>
## <div align="center"> Help desk:</div>
**Help:** <br>
`help <built-in command>`- displays information about shell built-in commands.<br>


**Manual:** <br>
`man <built-in command>` - system's manual.<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-f` - display a concise one-line description of the command.<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-k` - searches the given command as a regular expression in all the manuals
_Example:_ <br>
`man -f ls`<br>
`man -k file`<br>
_Navigating:_ <br>
`Spacebar`- move forward one page;
`Enter` - move forward one line;
`B` - move backward one page;
`Q` - quit the manual viewer.


## <div align="center"> Console:</div>
`whoami` - display the current username;<br> 
&nbsp;&nbsp;&nbsp;&nbsp;`-u` - displays the UID (User ID) instead of the username.<br>
`sudo -i` or `su` - interactive session of shell running by root.<br>
`sudo !!` - re-run previous command with `sudo`.<br>
`clear` or `CTRL+L` - clear the terminal screen.<br>
`history` or `CTRL+R` - recent used commands.<br>
`reset` - reset setting and output of shell command line.<br>
`<command_1> | <command_2>` - PIPE, connects the STDOUT (standard output) file descriptor of the first process to the STDIN (standard input) of the second.<br>

`cat <file_name>` - concatenate files and print on the standard output;<br>
`cat <file_name> | cut -d: -f2` - ;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`uniq` - don't show duplicates;<br>
`cat <file_name> | uniq | wc -l` - count only uniq lines;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`sort` - ;<br>
`cat <file_name> | sort -bf` - ;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`grep` - ;<br>
`cat <file_name> | grep user` - .<br>


`head <filename>` - outputs the first part of files; <br>
*Example:*
`head -n 4 filename.txt` - output first 4 ines of sample file; <br>
`head -c 20 filename.txt` - outputs first 20 bytes of sample file; <br>
`head -v filename.txt` - display name tag first alone other information; <br>
`head -n 4 filename_1.txt filename_2.txt` - output multiple files;<br>
`head filename.txt > output.txt` - redirect first 10 line of sample file to out file;<br>
`ls /etc | head` - to print first 10 entries of file list at /etc. <br>


`tail [options] <filename>` - prints data from the end of a specified file or files to standard output; <br>
`tail <filename>` - print last 10 lines of sample file; <br>
`tail -n 5 <filename>` - print last 5 lines of sample file; <br>
`tail -c 24 <filename>`- print last 24 bytes of sample file; <br>
`tail -q <filename>` - print last 10 lines without name of the sample file; <br>
`tail -n 1 <failename_1> -n 20 <filename_2>` - print last lines from multiple sample files; <br> 
`tail -n 7 <filename> | sort -R` - take the last five lines from the sample file and pipe the output to sort and randomize the order in result; <br>
`tail -n 12 <filename> > <output.txt>` - redirect the result of tail command to output file sample; <br>
`tail -f -s 5 <filname>` - track files in real time (with 5 sec interval) and show any updates in the standard output. <br>


**GREP (EGREP replacement)**<br>


**Environmental variables:** <br>
Defined in `/etc/profile`, `/etc/profile.d/` and `~/.bash_profile`. These files are the initialization files and they are read when bash shell is invoked.
When a login shell exits, bash reads `~/.bash_logout`.<br>
The startup is more complex; for example, if bash is used interactively, then `/etc/bashrc` or `~/.bashrc` are read.
**HOME**: The default argument (home directory) for cd;<br>
**PATH**: The search path for commands. It is a colon-separated list of directories that are searched when you type a command;<br>
**LOGNAME**:  contains the user name;<br>
**HOSTNAME**: contains the computer name.<br>


## <div align="center"> Files:</div>
**Navigation:** <br>
`pwd` - print name of current/working directory.<br>
`w` - show who is logged on and what they are doing.<br>

`cd <path>` - change current/working directory.<br>
`cd ..` - move to one level up;<br>
`cd ../..` - move to two level uo and etc;<br>
`cd -` - move to the beginning.<br>


`ls` - list files and directories of current/working directory.<br>
_Examples:_<br>
`ls -1 ./*.sh` - lists the shell script files in the current directory. 
`ls -alh` - show hiden files, use a long listing format and human-readable.<br>
&nbsp;&nbsp;&nbsp;&nbsp;&ndash;rwxrwxrwx - file;<br>
&nbsp;&nbsp;&nbsp;&nbsp;**d**rwxrwxrwx - directory;<br>
&nbsp;&nbsp;&nbsp;&nbsp;**l**rwxrwxrwx - link to other file/directory;<br>
&nbsp;&nbsp;&nbsp;&nbsp;**b**rwxrwxrwx - block (storage device);<br>
&nbsp;&nbsp;&nbsp;&nbsp;**c**rwxrwxrwx - character device file (mouse).<br>
*Execute permission on a folder allows to enter it and access files (or other directories) inside. The execute bit on a directory allows access items that are inside the directory, even if  cannot list the directories contents.*


`history 5` - show your five last commands;<br>


`touch <filename>` - creates an empty file;<br>
*Example:*
`touch {1..100}` - creates 100 files with 1,2,..100 file names;<br>
*If the file exists, the command changes creation date and adds new information to it additionally to existed ones.*


`test <directory_name>` - check if directory exists. <br>
`test -f <filename>` - check if file exists. <br>


`mkdir <directory_name_1> <directory_name_2> ...` - makes directories;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-p` - no error if existing, make parent directories as needed.<br>
*Example*:
`mkdir -p ./Home/{a,b}/{x,y,z}`<br>
```
./Home/a/
    ./Home/a/x/
    ./Home/a/y/
    ./Home/a/z/
./Home/b/
    ./Home/b/x/
    ./Home/b/y/
    ./Home/b/z/
```    

`rmdir <directory_name>` - remove the directory. The directory must be empty. <br>

`cp <source_file/dir_name> <destination_file/dir_name>` - copy of file/dir;<br> 
`cp -R <source_directory>/* <destination_directory>` - copy content of the directory recursively;<br>
`cp -R <source_directory_1> <source_directory_2> ... <source_directory_n>  <destination_directory>` - copy multiple directories.<br>

`mv <source_file/dir_name> <destination_file/dir_name>` - move of file.<br> 

`rm -r <file/dir_name>` - remove files or directories recursevly.<br>
_Example_: <br>
`sudo rm -rf /etc/docker` - force remove of docker directory.<br> 



`echo <text/file_name>` - printing text or content of the file.<br>

`df -ah` - report file system space usage;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-a` - all (include pseudo, duplicate, inaccessible file systems);<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-h` - human-readable;<br>
`df -i`<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-i` - list inode information instead of block usage.<br>
*Change the number of inodes in filesystem is possible only at creation of file system
`du` - estimate file space usage;<br>



**Permissions:** <br>
`chown <user_name> <file/dir_name_1> <file/dir_name_2> ...` - changes ownership of files to specified user/group;<br>
`chown --reference=<source_file/dir_name> <destination_file/dir_name>` - change owner based of the owner on aother file;<br>


`chmod [OPTIONS] <file/dir_name>` -  changes file/dir permissions to specified user/group;<br>
Attributes `rwxrwxrwx` means **R**ead/**W**rite/e**X**ecute for user, **R**ead/**W**rite/e**X**ecute for group of user, **R**ead/**W**rite/e**X**ecute for others;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`+` - add to; <br>
&nbsp;&nbsp;&nbsp;&nbsp;`-` - take away; <br>
&nbsp;&nbsp;&nbsp;&nbsp;`=` - replace with; <br>
&nbsp;&nbsp;&nbsp;&nbsp;`u` - for users; <br>
&nbsp;&nbsp;&nbsp;&nbsp;`g` - for group; <br>
&nbsp;&nbsp;&nbsp;&nbsp;`o` - for others; <br>
&nbsp;&nbsp;&nbsp;&nbsp;`a` (or nothing at all) - for all (users, group and others). <br>
`chmod u+r <file/dir_name>` -  adds permission to file/dir to be readable by user;<br>
`chmod a+x <file_name>` - adds permission to file to execute by anybody;<br>
`chmod -R o-r *.page` - takes away Read permission to all files with `.page` extension in all subfolders recursively;<br>


8-bit variant: <br>
`4` - Read. The file can be opened, and its content viewed;<br>
`2` - Write. The file can be edited, modified, and deleted;<br>
`1` - Execute. If the file is a script or a program, it can be run (executed).<br>

`chmod 764 <file/dir_name>` - adds to file/dir permissions, to owners `rwx`, to group `rw-` and to others `r--`.<br>


`chgrp -R <group_name> <dir_name>` - changes ownership of files in directory to specified group recursively.<br>


https://media.licdn.com/dms/image/D4E10AQHdPjNQbzH3hQ/image-shrink_800/0/1719694800998?e=1720940400&v=beta&t=_-Ufb29XnlKJSoQKyjv1kJVa58r2lIihZQnlw5_NbW8

**Archiving and compression on Linux:** <br>
`tar -zcvf <file_name>.tar.gz <source_file/dir>` - create archive;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-f` - define actual file/dir name;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-v` - verbose;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-z` - gzip;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-c` - create archive;<br>
`tar -zxvf <file_name>.tar.gz <destination_location>` -	extract archive;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-x` - extract;<br>
Extract group of files using wildcard: --wildcards
`tar -jxvf <file_name>.tar.bz2 --wildcards '<part_of_file_name>'`<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-j` - reate a highly compressed tar file;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`--wildcards` - extract group of files using wildcard.<br>
`tar -tvf <file_name>.tar`<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-t` - list content of an archive file;<br>
`tar -czf <file_name>.tar | wc -c` - check the size of created archive file.<br>


**Files editing:** <br>
**SED:** <br>
`sed -i s/foo/bar/g <file_name>` - replace "foo" with "bar";<br> 
`-i` - update file;<br>


**VIM:** <br>
https://www.youtube.com/watch?v=ZEIpdC_klDI
`vi`

**Hot Keys**:
[i] - input mode;
[Esc] - exit from input mode;
[d]+[d] - delete the line;

**To set showing numbers on line in VIM add this line**:
`vi ~/.vimrc`<br>
`set number`

File system check
e2fsck

Encryption
cryptsetup

## <div align="center"> Users:</div>
`useradd -m -d /home/<user_name> -s /bin/bash <user_name>` - 
&nbsp;&nbsp;&nbsp;&nbsp;`-m` - create Home directory;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-d` - define where it is;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-s` - define default shell;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-u` - define UID.<br>
`userdel` - delete user;<br>
`usermod` - modify user: <br>
`passwd <username>` - set a password;<br>
`passwd -e <user_name>` - this immediately expires the user’s password, requiring them to change it the next time they log in;<br>
`passwd -l <user_name>` - the password for the user is now locked, preventing login;<br>
`passwd -u <user_name>` - the password for the user is now unlocked, allowing login again;<br>
`passwd -a -S` - shows password status for all users.<br>


`groupadd [OPTIONS] <group_name>` - add group;<br>
`groupdel [OPTIONS] <group_name>` - remove group; <br>
`sudo usermod -aG <group_name> <user_name>` - add user to a group;<br>
`cat /etc/group` - show availible groups;


## <div align="center"> System:</div>
**Package Managers:** <br>
&nbsp;&nbsp;&nbsp;&nbsp;**Advanced Package Tool (APT):** package manager for Debian-based distributions (Ubuntu, Linux Mint, etc);<br>
`sudo apt-get update` - downloads package information from all configured sources;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`cat /etc/apt/sources.list` - show a list of sources.<br>
`sudo apt-get upgrade` - install available upgrades of all packages currently installed on the system from the sources configured via sources.list file;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`apt upgrade` - deletes packages that is not in use.<br>
`sudo apt-get install <app_name>` - install or upgrade packages;<br>
`sudo apt-get reinstall <app_name>` - resets package to its default state, by re-installing that package<br>
`sudo apt-cache search <app_name>` - search the local package cache for packages that match a given search term;<br>
`sudo apt-get remove <app_name>` - removes the packages, but it does not remove any configuration files created by these packages;<br>
`sudo apt-get purge <app_name>` - removes the packages, and also removes any configuration files related to the packages;<br>
`sudo apt-get clean` - removes all the cached package files that were downloaded due to the downloading of recent packages using `apt-get`;<br>
`sudo apt-get autoremove` -  removes the packages which are automatically installed to satisfy the dependencies of other packages and no longer needed;<br>
`sudo dpkg -i ~/Downloads/<app>.deb` - install from local file.<br> 


**Services:** <br>
service operates on the files in /etc/init.d and was used in conjunction with the old init system. systemctl operates on the files in /lib/systemd. If there is a file for your service in /lib/systemd it will use that first and if not it will fall back to the file in /etc/init.d. Also If you are using OS like ubuntu-14.04 only service command will be available.

So if systemctl is available ,it will be better to use it


&nbsp;&nbsp;&nbsp;&nbsp;**Cron:** <br>


## <div align="center"> Hardware:</div>



## <div align="center"> Hot Keys:</div>

Move cursor at begginning of line
Ctrl+A
Move cursor to the end of line
CTRL+E


## <div align="center"> Search:</div>
## <div align="left"> Find:</div>
`find ~ -name '*jpg'` - find files by name part;<br>
`find ~ -iname '*jpg'` - find files by name part with case-insensitive;<br>
`find ~ ( -iname 'jpeg' -o -iname 'jpg' )` - combine patterns: -o;<br>
`find ~ \( -iname '*jpeg' -o -iname '*jpg' \) -type f` - find only through files: -type f;<br>
`find ~ \( -iname '*jpeg' -o -iname '*jpg' \) -type d` - find only through directory: -type d;<br>
`find ~ \( -iname '*jpeg' -o -iname '*jpg' \) -type f -mtime -7` - modified time less than 7 days;<br>
`find /var/log -size +1G` - find all >1 GB files;<br>
`find /data -owner <user_name>` - find all files owned by <user_name>;<br>
`find ~ -perm -o=r` - find all world-readable file;<br>
`find . -type f -perm 0777 -print` - find all the files whose permissions are 777;<br>
`find / -perm /u=r` - find all Read Only files;<br>
`find / -perm /a=x` - find all Executable files;<br>
`find <path>/ -type d -iname '<dir_name>'` - find directories that matched a specific name.<br>



## <div align="center"> Logs:</div>
`tail <file_name>` - shows last 10 line of file;<br>
`tail -f <file_name>` - follow changes in the file and shows last 10 lines;<br>


## <div align="left"> Journalctl:</div>
# Filter output by message priorities
# 0: emergency; 1: alerts; 2: critical; 3: errors; 4: warning; 5: notice; 6: info; 7: debug
journalctl -p 0
# Messages for the specified service
journalctl -u *service_name* -u *service_name*
# Follow specified service log
journalctl -u *service_name* -f
# Messages related to a particular user
journalctl _UID=*
# To check UID
id *service_name*
# Messages logged within a specific time window
journalctl --since "2 days ago"
journalctl --since "1 hour ago"
journalctl --since "10 min ago"
journalctl --since "2015-06-26 23:15:00" --until "2015-06-26 23:20:00"
# Reverse chronological order to newest first
jourmalctl -r
# Kernel messages
journalctl -k
# Track logs from current system boot
journalctl -b 0
# Track logs from previous boot
journalctl -b -1
# List the boots of the system
journalctl --list-boots
# Disk space taken by logs
journalctl --disk-usage
# Clear logs
journalctl --vacuum-size=100M
journalctl --vacuum-time=2weeks
# Current limit status
systemctl status systemd-journald
# OR
journalctl -t systemd-journald
# Autodelete logs
# Edit /etc/systemd/journald.conf config file
SystemMaxUse=128M




## <div align="center"> Hardware:</div>
### <div align="left"> Network:</div>

**IP (IFCONFIG & ROUTE replacement)** <br>
`ip [options] [object] [command]`: 
Objects: <br>
    `link`,	`l`: network device;<br>
    `address`,      `a`/`addr`:	Protocol (IP or IPv6) address on a device;<br>
    `addrlabel`,    `addrl`: Label configuration for protocol address selection;<br>
    `neighbour`,    `n`/`neigh`: ARP or NDISC cache entry;<br>
    `route`,        `r`: Routing table entry;<br>
    `rule`,         `ru`: Rule in routing policy database;<br>
    `maddress`,     `m`/`maddr`: Multicast address;<br>
    `mroute`,       `mr`: Multicast routing cache entry;<br>
    `tunnel`,       `t`: Tunnel over IP;<br>
    `xfrm`,         `x`: Framework for IPsec protocol;<br>

`ip a` / `ip addr` - list and show all ip address associated on on all network interfaces;<br>
`ip -4 a`: only show TCP/IP IPv4;<br>
`ip -6 a`: only show TCP/IP IPv6;<br>

`ip a show eth0` / `ip a list eth0` / `ip a show dev eth0`: only show eth0 interface;<br>
`ip link show up`: only show running interfaces;<br>


`ip a add <ip_addr/mask> dev <interface>`: assigns the IP address to the interface;<br>
`ip a add 192.168.1.200/255.255.255.0 dev eth0` / `ip a add 192.168.1.200/24 dev eth0`: assign 192.168.1.200/255.255.255.0 to eth0;<br>

`ip a del <ipv6_addr/ipv4_addr> dev <interface>`: Remove / Delete the IP address from the interface;<br>
`ip a del 192.168.1.200/24 dev eth0`: delete 192.168.1.200/24 from eth0.<br>

`ip -s -s a f to 192.168.2.0/24`: flush the IP address from the interface;<br>

`ip link set dev <DEVICE> <up/down>`: change the state of the device to UP or DOWN;<br>
`ip link set dev eth1 up`: make the state of the device eth1 UP;<br>
`ip link set dev eth1 down`: make the state of the device eth1 DOWN;<br>

`ip link set txqueuelen <NUMBER> dev <DEVICE>`: change the txqueuelen of the device;<br>
`ip link set txqueuelen 10000 dev eth0`: change the default txqueuelen from 1000 to 10000 for the eth0.<br>

`ip link set mtu <NUMBER> dev <DEVICE>`: change the MTU of the device;<br>
`ip link set mtu 9000 dev eth0`: change the MTU of the device eth0 to 9000.<br>


`ip n show` / `ip neigh show`: Display neighbour/arp cache;<br>
Output options:
    `STALE`: The neighbour is valid, but is probably already unreachable, so the kernel will try to check it at the first transmission;
    `DELAY`: A packet has been sent to the stale neighbour and the kernel is waiting for confirmation;
    `REACHABLE` The neighbour is valid and apparently reachable.


`ip neigh add <IP-HERE> lladdr <MAC/LLADDRESS> dev <DEVICE> nud <STATE>`: add a new ARP entry;<br>
Neighbour states:<br>
    `permanent`: the neighbour entry is valid forever and can be only be removed administratively;<br>
    `noarp`: the neighbour entry is valid. No attempts to validate this entry will be made but it can be removed when its lifetime expires;<br>
    `stale`: the neighbour entry is valid but suspicious. This option to ip neigh does not change the neighbour state if it was valid and the address is not changed by this command;<br>
    `reachable`: the neighbour entry is valid until the reachability timeout expires;<br>
`ip neigh add 192.168.1.5 lladdr 00:1a:30:38:a8:00 dev eth0 nud perm`: add a permanent ARP entry for the neighbour 192.168.1.5 on the device eth0;<br>
`ip neigh chg 192.168.1.100 dev eth1 nud reachable`: change are state to reachable for the neighbour 192.168.1.100 on the device eth1.<br>

`ip neigh del <IPAddress> dev <DEVICE>`: Delete a ARP entry;<br>
`ip neigh del 192.168.1.5 dev eth1`: invalidate or delete an ARP entry for the neighbour 192.168.1.5 on the device eth1.<br>

`ip -s -s n f <IPAddress>`: flush ARP entry;<br>
`ip -s -s n f 192.168.1.5` / `ip -s -s n flush 192.168.1.5`: flush neighbour/arp table.<br>

`ip r list [options]`: show routing table;<br>
`ip r list 192.168.1.0/24`: display routing for 192.168.1.0/24.<br>

`ip route add <NETWORK/MASK> via >GATEWAY_IP>`: route to network via gateway;<br>
`ip route add <NETWORK/MASK> dev <DEVICE>`: route all traffic via gateway connected via eth0 network;<br>
`ip route add <NETWORK/MASK> via >GATEWAY_IP>`: set default route to network via gateway;<br>
`ip route add <NETWORK/MASK> dev <DEVICE>`: set by deafult to route all traffic via gateway connected via eth0 network;<br>
`ip route add 192.168.1.0/24 via 192.168.1.254`: add a plain route to network 192.168.1.0/24 via gateway 192.168.1.254;<br>
`ip route add 192.168.1.0/24 dev eth0`: route all traffic via 192.168.1.254 gateway connected via eth0 network.<br>

**SS (NETSTAT replacement)** <br>
`sudo ss -tulpn` - print network connections, routing tables, interface statistics, masquerade connections, and multicast memberships;<br>
`-n`: show numerical addresses instead of trying to determine symbolic host, port or user names;<br>
`-l`: show only listening sockets;<br>
`-p`: show connections or statistics only for a particular protocol;<br>
    `-t`: display TCP sockets;<br>
    `-u`: display UDP sockets;<br>
`sudo ss -tulpn | egrep "http"`: show only HTTP connections.<br>
`ss -tunapl | egrep "(ESTAB|LISTEN)"`: show only established and listening sockets


**DIG (NSLOOKUP replacement)** <br>
`dig [@server] [domain] [query-type] [options]` (Domain Information Groper command) is a network tool that serves for making different DNS (domain name system) queries.<br>
    `@server`: This is optional. Use it to specify the DNS server you want to query. If omitted, DIG uses the default server;<br>
    `domain`: This is the domain name you are querying about;<br>
    `query-type`: This specifies the type of DNS record you want to query (e.g., A, MX, NS). If not specified, DIG defaults to querying A records;<br>
    `options`: DIG offers various options to format or filter query results. Here are some common options used with dig:<br>
        `+short`: Gives a shorter, more concise output;<br>
        `+trace`: Traces the path of the query across the DNS namespace;<br>
        `+noall +answer`: Shows only the answer section of the query;<br>
`dig example.com`: show the website’s IP address. Looks for the A records, as they have the IP addresses which correspond to the domain name form the query;<br>
`dig -x 10.2.33.124`: with which IP address a domain name is assciated with;<br>
`dig NS example.com +short`: who is responsible for this domain? Shows all the name servers, in a list, for the particular domain;<br>
`dig NS com +short`: shows which are the name servers, responsible for the TLDs (top-level domains), in this case "com";<br>
`dig example.com +nssearch`: check if your DNS zone is synchonized over all authoritative name servers. Checks the SOA records and see if their data matches. If your DNS zones are not synchronized, you will need to manually manage and update them;<br>
`dig example.com +trace`: shows the delegation patch from the root server to this DNS zone;<br>
`dig MX example.com +short`: check the responsible mail servers for accepting emails;<br>
`dig example.com +noall +answer`: check when the cache of an answer will expire;<br>
`dig SOA example.com @ns1.example.com`: checks if a zone exists on a name serve;<br>
*Possible answers:*<br>
    `NOERROR` – yes, the zone exists;<br>
    `NXDOMAIN` – no, it does not;<br>
    `REFUSED` – the name server does not want to answer.<br>
`dig example.com @8.8.8.8`: checks what a particular resolver (Google’s DNS resolver: 8.8.8.8) has in its cache memory.<br>


Online tool for DIG:
https://www.cloudns.net/tools/


`strace -ff -e trace=open,stat -p 20638`

`gdb -p 23998`

`ps ax | grep nginx | grep master`

`tcpdump -nn -i lo -A port 8000`


host 


hostname




# <div align="center"> Third-party commands:</div>
## <div align="center"> Help desk:</div>
[Simplified manuals](https://ostechnix.com/3-good-alternatives-man-pages-every-linux-user-know/)<br>


## <div align="center"> Console:</div>
&nbsp;&nbsp;&nbsp;&nbsp;**TMUX:** <br>
A quick cheatsheet (partially inspired by http://www.danielmiessler.com/study/t... ):

**session management** <br>
tmux ls (or tmux list-sessions)
tmux new -s session-name
Ctrl-b d Detach from session
tmux attach -t [session name]
tmux kill-session -t session-name

Ctrl-b c Create new window
Ctrl-b d Detach current client
Ctrl-b l Move to previously selected window
Ctrl-b n Move to the next window
Ctrl-b p Move to the previous window
Ctrl-b & Kill the current window
Ctrl-b , Rename the current window
Ctrl-b q Show pane numbers (used to switch between panes)
Ctrl-b o Switch to the next pane
Ctrl-b ? List all keybindings

**moving between windows:** <br>
Ctrl-b n (Move to the next window)
Ctrl-b p (Move to the previous window)
Ctrl-b l (Move to the previously selected window)
Ctrl-b w (List all windows / window numbers)
Ctrl-b window number (Move to the specified window number, the
default bindings are from 0 -- 9)

**Tiling commands:** <br>
Ctrl-b % (Split the window vertically)
CTRL-b " (Split window horizontally)
Ctrl-b o (Goto next pane)
Ctrl-b q (Show pane numbers, when the numbers show up type the key to go to that pane)
Ctrl-b { (Move the current pane left)
Ctrl-b } (Move the current pane right)

**Make a pane its own window:** <br>
Ctrl-b : "break-pane"

**add to ~/.tmux.conf:** <br>
bind | split-window -h
bind - split-window -v


## <div align="center"> Files:</div>
`rsync -ar <source_directory> <destination_user>@<destination_host>:<destination_directory>` -  copy to remote host;<br>
`rsync -ar /etc/* ubuntu@192.168.1.2:/etc_backup/etc_$(date "+%F")` - copy content of the directory to the remote host with dynamicly named directory.<br>


`scp -r -v -P 2222 <user_name>@<source_host>:<source_directory>/*  <user_name>@<destination_host>:<destination_directory>`<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-r` - recursively copy entire directories;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-P <port>` - specify the port to connect to on the remote host;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-v` - enable verbose mode for detailed output during the transfer;<br>
`scp -i ./ssh/aws.key -C -p -l 800 <user_name>@<source_host>:<source_file> <user_name>@<destination_host>:<destination_directory>`<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-i <identity_file_name>` - specify the private key file for authentication;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-C` - compress the file while it's being transferred;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-p` - preserve file attributes such as modification and access times, modes, and permissions;<br>
&nbsp;&nbsp;&nbsp;&nbsp;`-l <number_KB/s` - limit the bandwidth.<br>



## <div align="center"> Hardware:</div>

**Change MAC-address:** <br>
`sudo apt-get install macchanger`<br>
`sudo ip link set dev wlp2s0 down`<br>
`sudo macchanger -r wlp2s0`<br>
`sudo ip link set dev wlp2s0 up`<br>
or<br>
`sudo ip link set dev wlp2s0 down`<br>
`sudo ip link set dev wlp2s0 address <MAC-address>`<br>
`sudo ip link set dev wlp2s0 up`<br>


## <div align="center"> Processes:</div>
ps

top

kill

nice

systemctl

bg

fg



### <div align="left"> Procmon for Linux:</div>
Process Monitor for Linux (SysInternals for Linux). But instead of Windows Event logs it uses Linux Syslog.<br>
[ProcMon-for-Linux](https://github.com/Sysinternals/ProcMon-for-Linux)<br>
*for Ubuntu

### <div align="left"> Glances:</div>
[Glances:  open-source system cross-platform monitoring tool](https://github.com/nicolargo/glances)<br>


## <div align="center"> Logs:</div>
https://perfecto25.medium.com/monit-alert-system-config-examples-a7967d6a1e1f

# Monit 
# Source - https://www.youtube.com/watch?v=wiRt3mY7Rrw
# monit rules
set daemon 30
set logfile /var/log/monit.log

# e-mail alerts
ser alert d.zuyenko@gmail.com

set httpd
	port 2812
	user address localhost		# only accept connection from localhost
	# allow localhost			# allow localhost to connect to the server
	allow ausername:User0000

# check mysql
cheack host mysqldb with address 127.0.0.1
	if failed ping then alert
	if failed port 3306 protocol mysql then alert

# nginx
cheack procces nginx with pidfile /var/run/nginx.pid
	start program = "/etc/init.d/nginx start"
	stop program = "/etc/init.d/nginx stop"
	group www-data

# php-fpm
cheack process phpfpm with pidfile /var/run/php-fpm.pid
	if cpu >50% for 2 cycles then alert
	# if total cpu > 60% for 5 cycles then restart
	if memory > 400 MB then alert
	# if total memory > 500 MB then restart

# include files for individual sites
Include /etc/monit/monit.d/*cfg



## <div align="center"> Search:</div>

## <div align="center"> Scripting:</div>

## <div align="center"> System:</div>
sudo apt-get install gnome-tweak-tool

## <div align="center"> Processes:</div>




## <div align="center"> Network:</div>

## <div align="center"> CURL</div> 

**CURL request 
```
curl --location 'https://<URL>' \
--header '<key>: <value>' \
--header 'Authorization: Bearer <JWT_token>' 
```

`--location` - follow any HTTP 3xx redirects

_**Example**_:<br>
```
curl --location 'https://<int.apigw.umbrella.com/admin/v2/cloudApplicationInstances/366/resources>' \
--header 'x-umbrella-orgid: 7975099' \
--header 'Authorization: Bearer <TOKEN>' 
```

## <div align="center"> Sessions & Authentication:</div>

**To connect to GitHub via SSH (on ARM-based Mac)**:
1. Add line to the `.zprofile`:<br>
`nano ~/.zprofile`
```
export DOCKER_DEFAULT_PLATFORM=linux/amd64
export GIT_USER=dzuienko                                 
export GIT_TOKEN=<GITHUB_TOKEN>
```
2. Config `git` to use SSH instead of HTTPS:<br>
`git config --global --add url."git@github.com:".insteadOf "https://github.com/"`
3. Generate public and private RSA SSH key and add it to GitHub:
`ssh-keygen -t ed25519 -C "dzuienko@cisco.com"` 
4. Add RSA public key to Github account: click your profile photo > [Settings] > [SSH and GPG keys] > [New SSH key]:
`cat ~/.ssh/<id_rsa>.pub`
5. Add private RSA SSH key to the SSH agent to passwordless access:
`cd ~/.ssh`
`ssh-add id_ed25519`
6. Add GitHub's server to SSH client as known and trusted: 
`ssh-keyscan -t rsa github.com >> ~/.ssh/known_hosts`
Check added keys:<br>
`ssh-add -l`<br>
`ssh-add -L`
Delete all added keys:<br>
`ssh-add -D`
7. Initiate authorization session 
`ssh -vT git@github.com`

