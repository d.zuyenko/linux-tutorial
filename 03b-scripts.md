https://www.shellscript.sh/

&nbsp;&nbsp;&nbsp;&nbsp;**Scripting:** <br>
**Navigating:** <br>
`..` - parent directory; <br>
`../..` - parent of parent directory; <br>
`.` - current directory; <br>
`~/` - shortcut to a user's home directory; <br>
`/` - root directory. <br>

&nbsp;&nbsp;&nbsp;&nbsp;**Test operators:** <br>
**File operators:** <br>
`-f` - if a file;<br>
`-d` - if a directory;<br>
`-nt` - newer than;<br>
`-x` - executable;<br>
`-r` - readable;<br>
`-w` - writable.<br>

**Logical operators:** <br>
`||` - OR;<br>
`&&` - AND;<br>
`!` - NOT.<br>

**Numeric operators:** <br>
`==` or `-eq` - equal;<br>
`!=` or `-ne` - not equal;<br>
`>` or `-gt` - greater then;<br>
`>=` or `-ge` - greater then or equal;<br>
`<` or `-lt` - less then;<br>
`<=` or `-le` - less then or equal.<br>

**String operators:** <br>
`=` - equal to;<br>
`z` - zero lengh;<br>
`n` - not zero lengh.<br>

**Arguments reference:** <br>
$# - number of args that script was run with
$0 - the filename of a script
$1-$n - number of argument
$* - all arguments
echo $SHELL - what shell am i on
echo $$ - PID of current process
echo $? - return code of last command
echo $! - PID of last background command
*Every Linux or Unix command executed by the shell script or user has an exit status. Exit status is an integer number. 0 exit status means the command was successful without any errors. A non-zero (1-255 values) exit status means command was a failure.*

**$PATH:** <br>
echo $PATH - directories that contains commands, utils and etc
PATH=$PATH:/*directory_name* - append to $PATH list
PATH=/*directory_name*:$PATH - append to $PATH list at the beginning

**Standard streams shortcuts:** <br>
0: STDIN (Input); 1: STDOUT (Output); 2: STDERR (Error)

Redirect STDOUT to a file, create new file if it not exist or truncate it to zero size
*filename/command* > *filename*

Create the file
> *filename*

Append new data at the end of file
*filename/command* >> *filename*

STDOUT from command_2 to STDIN of command_1
*command_1*<<<*command_2*

Add second type of standard stream
1>&2

Display STDERR and STDOUT on the console and in a file
*command* 2>&1 | tee *filename*

`\` - bash escape character and it preserves the literal value of the next character that follows.<br>


BASH-scripts
# Path to bash in the system, every bash script should begin with this line
#!/bin/bash

# Comment
# *any_text*

# Install, start, config, run Apache web server
#!/bin/bash

sudo yum install httpd
sudo service httpd on
sudo chkconfig httpd on

sudo touch /var/www/html/index.html

sudo chmod 777 /var/www/html/index.html

echo "<h1>This app is deployed!</h1>" > /var/www/html/index.html


# Check where redirection is coming from
#!/bin/bash

if [ -t 0 ]; then
  echo stdin coming from keyboard
else
  echo stdin coming from a pipe or a file
fi
# Make the script executable
chmod +x input.sh
# Run command with redirection
./input.sh < *exist_file*
# Run pipe with redirection
cat *exist_file* | ./input.sh


# Check where redirection is going to
#!/bin/bash

if [ -t 1 ]; then
echo stdout is going to the terminal window
else
echo stdout is being redirected or piped
fi
# Make the script executable
chmod +x output.sh
# Run command with redirection
./output.sh > *exist_file*
# Run pipe with redirection
./output | cat


# Change group permissions on every file in directory
#!/bin/bash

for i in $(ls -1)
do
	chgrp jason ${i}
done


# Exit with last argument
#!/bin/bash

message="Hello World!"
echo $message

exit $?


# IF operator example
#!/bin/bash

NAME=$1
GREETING="Hi there"
HAT_TIP="*tip of the hat*"
HEAD_SHAKE="*slow head shake*"

if [ "$NAME" = "Dave" ]; then
	echo $GREETING
elif [ "$NAME" = "Steve" ]; then
	echo $HAT_TIP
else
	echo $HEAD_SHAKE
fi
# Run the script with argument 'Dave', 'Steve' or other

# Example with IF, FOR, FUNCTION
#!/bin/bash

NUM_REQUIRED_ARGS=2
num_args=$#

# Check required number of arguments
if [ $num_args -lt $NUM_REQUIRED_ARGS ]; then
	echo "Not enough arguments. Call this script with ${0} <name> <number>"
	exit 1
fi

name=$1
number=$2
echo "Your first two arguments were: $1 $2"

# For loops, iteration, string interpolation
echo "You ran this program with ${num_args} arguments. Here they are:"
for arg in "$@"; do
	echo "$arg"
done

# Two ways of defining a function
spaced() {
	# Parameters are not named; they are positional
	echo
	echo "#################"
	echo "$1"
	echo "#################"
	echo 
}

# Define a function
function javatest() {
	# Testing and conditionals
	if [[ $number -eq 99 ]]; then
		spaced "You win! You guesses the secret number! It’s amazing"
	elif (( $number < 10 )); then
		spaced "You’re a courageous one. I like that about you. Unfortunately, you must die"

		# Set a variable interactively
		echo "Hi ${name}, to avert a horrible death, please enter the password:"
		read password

		if [[ "$password" != "Java" ]]; then
			spaced "Well, at least you’re not a Java Programming sympathizer. You can go now."
		else
			spaced "DIE! Actually, nevermind. Writing Java is enough \
of a hellish punishment. You are free to leave. Take a biscuit on the \
way out."
		fi
	fi
}

javatest $number
exit 0



https://habr.com/ru/company/southbridge/blog/222469/




Regular expressions, often abbreviated as regex or regexp, are powerful tools for pattern matching and text manipulation. Let me explain them in simple terms:
What are regular expressions for?
Regular expressions are used to search, match, and manipulate text based on specific patterns. They provide a concise and flexible way to identify strings or parts of strings that conform to a particular structure.
Types of regular expressions:
While there isn't a formal classification of regex types, we can categorize them based on their complexity and functionality:

Simple patterns: Basic character matching
Character classes: Matching sets of characters
Quantifiers: Specifying repetitions
Anchors: Matching positions in text
Grouping and capturing: Organizing and extracting parts of matches
Lookarounds: Advanced pattern matching without consuming characters

Common uses of regular expressions:

Data validation: Checking if input matches a specific format (e.g., email addresses, phone numbers)
Data extraction: Pulling out specific information from text (e.g., dates, URLs)
Search and replace: Finding and modifying text patterns in documents
Parsing: Breaking down structured text into meaningful components
Filtering: Selecting or excluding items based on patterns
Text processing: Manipulating strings in various ways

Here are some simple examples to illustrate regex usage:

Matching an email address:
[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}

### Makefile

.PHONY

init


build:
	@docker build --build-arg GOPRIVATE_USER=${GIT_USER} --build-arg GOPRIVATE_TOKEN=${GIT_TOKEN} --tag core-platform/upstream-authorizer-service .
	docker tag core-platform/upstream-authorizer-service:latest 430993202893.dkr.ecr.us-west-2.amazonaws.com/core-platform/upstream-authorizer-service:latest


example and tips


