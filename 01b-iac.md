**CONTENTS**

[[_TOC_]]


# **<div align="center">Configuration management tools</div>**
**Infrastructure as code (IaC)** tools allow you to manage infrastructure with configuration files rather than through a graphical user interface. IaC allows you to build, change, and manage your infrastructure in a safe, consistent, and repeatable way by defining resource configurations that you can version, reuse, and share.<br>

## **<div align="left">Terraform (Hashicorp):</div>**
- Terraform can manage infrastructure **on multiple cloud platforms**;
- The **human-readable configuration** language helps you write infrastructure code quickly;
- Terraform's state allows you to **track resource changes** throughout your deployments;
- You can **commit your configurations to version control** to safely collaborate on infrastructure.

To deploy infrastructure with Terraform:<br>
- **Scope** - Identify the infrastructure for your project;
- **Author** - Write the configuration for your infrastructure;
- **Initialize** - Install the plugins Terraform needs to manage the infrastructure;
- **Plan** - Preview the changes Terraform will make to match your configuration;
- **Apply** - Make the planned changes.

**Provider** - plugin that Terraform uses to create and manage your resources. For example, "aws".<br>

**Resource** - resource might be a physical or virtual component such as an EC2 instance, or it can be a logical resource such as a Heroku application. Resource blocks have two strings before the block: the resource type and the resource name, like "aws_instance.app_server".<br>

**Module** - set of Terraform configuration files in a single directory. Even a simple configuration consisting of a single directory with one or more .tf files is a module. When you run Terraform commands directly from such a directory, it's considered the root module.<br>



state-list: check-api-env ## Run ENV and API specific terraform plan
	cd ${API} && ${TERRAFORM_PATH} state list

ENV=stage API=policy-api make state-list

state-rm: check-api-env ## Run ENV and API specific terraform plan
	cd ${API} && ${TERRAFORM_PATH} state rm "module.policy-api.kong_plugin.v2_app_portals_id_rate_limiting"

ENV=stage API=policy-api make state-rm

https://spacelift.io/blog/terraform-conditionals