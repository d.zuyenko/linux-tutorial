

# **<div align="center">Google Kubernetes Engine:</div>**
**The core components of Container as a Service:**
- **A group of Google Compute Engine instances** that run Kubernetes;
- **A master node** which manages a cluster of containers, and runs a Kubernetes API;
- **Additional cluster nodes** running a kubelet agent and runtime, both of which are essential to manage containers.
You can organize groups of pods into services. This enables non-container-aware applications to gain access to other containers without any additional code.
