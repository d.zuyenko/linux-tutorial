**CONTENTS**

[[_TOC_]]


# **<div align="center">Softskills</div>**
**Integrity** *(adequacy)* <br>
Integrity is the deepest and most core principle, encompassing moral, intellectual, personal, and corporate integrity. Integrity requires a consistency of our thoughts, words, and actions and a dedication to the truth.

**Humility** *(don't try to appear like you know when you don't)* <br>
Humility starts with acknowledging that our knowledge is imperfect and incomplete, but not fixed. We can continue to learn and grow but this is an active process that we must choose to engage in. This growth comes from constantly seeking feedback, learning, and adapting based on new understanding. In this context, we must view mistakes as learning opportunities in an active process of reflection and analysis.

**Kindess** *(the banality of good)* <br>
In the face of our own personal frustration, it is often difficult to remember that our actions will be received by another thoughtful, emotional human being. We should assume the best in people, communicate kindly, and understand that the intention of another’s actions are usually to be helpful in return. In some cases, we may be the recipients of unkindness. We always choose to respond with kindness, in the hope that we can move towards a better communication environment. If this isn’t possible, you may choose to exit the conversation. We can’t change the unkindness of others, but we can preserve the kindness of ourselves.

**Execution** <br>
The execution of an idea matters much more than the idea itself. This means that the best idea poorly executed is no better than a mediocre idea well executed. Action should always be preferred to inaction and uncertainty around the best idea must not prevent execution.


https://www.linkedin.com/posts/ford-coleman_8-soft-skills-to-accelerate-your-career-ugcPost-7247633301887234048-EWbi