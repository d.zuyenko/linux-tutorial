Mac...

GoLang
export PATH=$PATH:/usr/local/go/bin

### Update Go package
1. Edit go.mod file
2. Run 'go get <go_package_name>
3. Run 'go mod tidy
4. Commit everything 





Remove GO from Mac
sudo rm -rf /usr/local/go
sudo rm /etc/paths.d/go

Give another go at force removing the brewed version of git
brew uninstall --force git
Then cleanup any older versions and clear the brew cache
brew cleanup -s git
Remove any dead symlinks
brew cleanup --prune-prefix
Then try reinstalling git
brew install git



To delete your history, you can type history -p
To see the last n commands, you can use history -n. For example, to see the last 905 commands, you would type history -905. 
To always see your full history, you can add an alias to the .zshrc file. For example, you can add alias history='history 1'