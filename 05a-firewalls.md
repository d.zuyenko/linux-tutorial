# <div align="center"> Firewalls</div>

Firewall
A firewall is a network security device that monitors incoming and outgoing network traffic and permits or blocks data packets based on a set of security rules. Its purpose is to establish a barrier between your internal network and incoming traffic from external sources (such as the internet) in order to block malicious traffic like viruses and hackers.



Firewall:
Firewalld is a frontend for nftables (former iptables).
UFW (with GUI GUFW)

pfSense

Cisco ASA
Stateful inspection
Allow in only following opened session trafic (by invite)


Cisco IDS/IPS

Intrusion prevention system (IPS), based on SNORT 3 technology, uses signature-based detection to examine network traffic flows and take automated actions to catch and drop dangerous packets before they reach their target.




**Umbrella’s cloud-delivered firewall (CDFW)**:
Umbrella's cloud-delivered firewall (CDFW) provides firewall services without the need to deploy, maintain, and upgrade physical or virtual appliances at a site. The Umbrella CDFW supports visibility and control of internet traffic across branch offices. Umbrella logs all network activity and blocks unwanted traffic using IP, port, and protocol rule criteria.

The firewall policy describes the active configuration of the Umbrella CDFW and Intrusion Prevention System (IPS). You can create any number of rules in a firewall policy to control the traffic in your networks. Within a firewall policy, configure the Umbrella IPS settings to detect or block threats and attacks. 

To forward traffic to Umbrella, establish an IPsec (Internet Protocol Security) IKEv2 (Internet Key Exchange, version 2) tunnel from any network device. Then, deploy the network tunnel in Umbrella and add the tunnel to an Umbrella firewall policy. As you add new tunnels to a firewall policy, Umbrella automatically applies and consistently enforces the rules defined in the policy.