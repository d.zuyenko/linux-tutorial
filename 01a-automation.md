**CONTENTS**

[[_TOC_]]


# **<div align="center">Jenkins:</div>**
**Jenkins** is a self-contained, open-source automation server that can be used to automate all sorts of tasks related to building, testing, and delivering or deploying software.


**Jenkins Architecture**:
- **Jenkins Server (Controller)**: manages the pipeline and associated jobs, stores the results;
- **Jenkins Agent(s)**: executes the job.
*Antipattern for production environment: when the server is the agent.*

https://medium.com/devops-guides/how-to-install-jenkins-on-ubuntu-24-04-in-aws-ec2-instance-0386d7c7a273




sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
  https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
echo "deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc]" \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
sudo apt-get update
sudo apt-get install jenkins

sudo apt update
sudo apt install fontconfig openjdk-17-jre
java -version







sudo sh -c 'echo deb [signed-by=/usr/share/keyrings/jenkins.gpg] http://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins
sudo ufw allow 8080
sudo ufw allow OpenSSH
sudo ufw enable
sudo ufw status


jenkins -v



sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update

sudo apt install docker-ce
# sudo systemctl status docker
sudo usermod -aG docker ${USER}

sudo reboot

sudo docker run -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts

sudo docker ps -a
sudo docker logs *container_ID*


https://github.com/vdespa/install-jenkins-docker
docker exec -it a7bd45643f31 sh



http://your_server_ip_or_domain:8080

[Install suggested plugins]

https://valaxytech.medium.com/multibranch-pipeline-on-jenkins-with-webhook-a65decede4f8
http://3.86.66.8:8080//multibranch-webhook-trigger/invoke?token=MF8e6iOU4l31DdEqkkYbFE

WEBHOOK TO GITHUB
https://www.blazemeter.com/blog/how-to-integrate-your-github-repository-to-your-jenkins-project


# **<div align="center">Packer:</div>**
**Packer** automates the creation of customized images in a repeatable manner.




# **<div align="center">Ansible:</div>**
**Ansible** is agentless automation tool working via SSH:
- No deployment effort at beginning
- No upgrade effort


**Playbook** is a collection of one or more plays that are performed in a certain order.<br>
**Play** is an ordered sequence of tasks performed against hosts from your inventory.<br>
**Collection** include playbooks, roles, modules, and plugin.<br>
**Role** provide a structured way to organize tasks, templates, files, and variables.<br>
**Task** to be done is defined by plays.<br>
**Module** is reusable, standalone script that runs on your behalf, locally or remotely.<br>
**Ansible inventory list** contains list of all servers (/etc/ansible/hosts), that allows to group related servers into group.<br>


# **<div align="center">Chef:</div>**
# **<div align="center">Puppet:</div>**
